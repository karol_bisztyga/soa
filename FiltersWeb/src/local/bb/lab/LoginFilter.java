package local.bb.lab;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		Login log = (Login) req.getSession().getAttribute("login");
		boolean doFilter = true;
		if(log == null || !log.isLoggedIn()) {
			if(req.getRequestURI().indexOf("secret") >= 0) {
				resp.sendRedirect(req.getServletContext().getContextPath() + "/index.xhtml");
				doFilter = false;
			}
		}
		if(doFilter) {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
	
	@Override
	public void destroy() {
		
	}

}
