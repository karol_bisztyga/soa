package local.bb.lab;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name="login")
@SessionScoped
public class Login implements Serializable {
	
	private String logged;
	private String userName;
	private String password;
	
	public void login() {
		if(userName == null || password == null) {
			logged = null;
			return;
		}
		if(!userName.equals("asd") || !password.equals("asd")) {
			logged = null;
			return;
		}
		logged = userName;
	}
	
	public void logout() {
		userName = null;
		password = null;
		logged = null;
	}
	
	public boolean isLoggedIn() {
		return (logged != null);
	}

	public String getLogged() {
		return logged;
	}

	public void setLogged(String logged) {
		this.logged = logged;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
