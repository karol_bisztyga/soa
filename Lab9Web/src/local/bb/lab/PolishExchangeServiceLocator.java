/**
 * PolishExchangeServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package local.bb.lab;

public class PolishExchangeServiceLocator extends org.apache.axis.client.Service implements local.bb.lab.PolishExchangeService {

    public PolishExchangeServiceLocator() {
    }


    public PolishExchangeServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PolishExchangeServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PolishExchangePort
    private java.lang.String PolishExchangePort_address = "http://localhost:4572/lab9/exchange";

    public java.lang.String getPolishExchangePortAddress() {
        return PolishExchangePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PolishExchangePortWSDDServiceName = "PolishExchangePort";

    public java.lang.String getPolishExchangePortWSDDServiceName() {
        return PolishExchangePortWSDDServiceName;
    }

    public void setPolishExchangePortWSDDServiceName(java.lang.String name) {
        PolishExchangePortWSDDServiceName = name;
    }

    public local.bb.lab.interfaces.Exchange getPolishExchangePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PolishExchangePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPolishExchangePort(endpoint);
    }

    public local.bb.lab.interfaces.Exchange getPolishExchangePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            local.bb.lab.PolishExchangePortBindingStub _stub = new local.bb.lab.PolishExchangePortBindingStub(portAddress, this);
            _stub.setPortName(getPolishExchangePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPolishExchangePortEndpointAddress(java.lang.String address) {
        PolishExchangePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (local.bb.lab.interfaces.Exchange.class.isAssignableFrom(serviceEndpointInterface)) {
                local.bb.lab.PolishExchangePortBindingStub _stub = new local.bb.lab.PolishExchangePortBindingStub(new java.net.URL(PolishExchangePort_address), this);
                _stub.setPortName(getPolishExchangePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PolishExchangePort".equals(inputPortName)) {
            return getPolishExchangePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://lab.bb.local/", "PolishExchangeService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://lab.bb.local/", "PolishExchangePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PolishExchangePort".equals(portName)) {
            setPolishExchangePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
