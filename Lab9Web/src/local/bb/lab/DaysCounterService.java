/**
 * DaysCounterService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package local.bb.lab;

public interface DaysCounterService extends javax.xml.rpc.Service {
    public java.lang.String getDaysCounterPortAddress();

    public local.bb.lab.interfaces.Counter getDaysCounterPort() throws javax.xml.rpc.ServiceException;

    public local.bb.lab.interfaces.Counter getDaysCounterPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
