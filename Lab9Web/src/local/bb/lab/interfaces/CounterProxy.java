package local.bb.lab.interfaces;

public class CounterProxy implements local.bb.lab.interfaces.Counter {
  private String _endpoint = null;
  private local.bb.lab.interfaces.Counter counter = null;
  
  public CounterProxy() {
    _initCounterProxy();
  }
  
  public CounterProxy(String endpoint) {
    _endpoint = endpoint;
    _initCounterProxy();
  }
  
  private void _initCounterProxy() {
    try {
      counter = (new local.bb.lab.DaysCounterServiceLocator()).getDaysCounterPort();
      if (counter != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)counter)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)counter)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (counter != null)
      ((javax.xml.rpc.Stub)counter)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public local.bb.lab.interfaces.Counter getCounter() {
    if (counter == null)
      _initCounterProxy();
    return counter;
  }
  
  public long getHolidaysInterval() throws java.rmi.RemoteException{
    if (counter == null)
      _initCounterProxy();
    return counter.getHolidaysInterval();
  }
  
  
}