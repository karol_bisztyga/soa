package local.bb.lab.interfaces;

public class ExchangeProxy implements local.bb.lab.interfaces.Exchange {
  private String _endpoint = null;
  private local.bb.lab.interfaces.Exchange exchange = null;
  
  public ExchangeProxy() {
    _initExchangeProxy();
  }
  
  public ExchangeProxy(String endpoint) {
    _endpoint = endpoint;
    _initExchangeProxy();
  }
  
  private void _initExchangeProxy() {
    try {
      exchange = (new local.bb.lab.PolishExchangeServiceLocator()).getPolishExchangePort();
      if (exchange != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)exchange)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)exchange)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (exchange != null)
      ((javax.xml.rpc.Stub)exchange)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public local.bb.lab.interfaces.Exchange getExchange() {
    if (exchange == null)
      _initExchangeProxy();
    return exchange;
  }
  
  public java.lang.Double count(local.bb.lab.interfaces.Currency arg0, double arg1) throws java.rmi.RemoteException{
    if (exchange == null)
      _initExchangeProxy();
    return exchange.count(arg0, arg1);
  }
  
  public java.lang.Double getRate(local.bb.lab.interfaces.Currency arg0) throws java.rmi.RemoteException{
    if (exchange == null)
      _initExchangeProxy();
    return exchange.getRate(arg0);
  }
  
  public local.bb.lab.interfaces.Currency[] getCurrencies() throws java.rmi.RemoteException{
    if (exchange == null)
      _initExchangeProxy();
    return exchange.getCurrencies();
  }
  
  public local.bb.lab.interfaces.Currency getCurrency() throws java.rmi.RemoteException{
    if (exchange == null)
      _initExchangeProxy();
    return exchange.getCurrency();
  }
  
  
}