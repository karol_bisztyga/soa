/**
 * Exchange.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package local.bb.lab.interfaces;

public interface Exchange extends java.rmi.Remote {
    public java.lang.Double count(local.bb.lab.interfaces.Currency arg0, double arg1) throws java.rmi.RemoteException;
    public java.lang.Double getRate(local.bb.lab.interfaces.Currency arg0) throws java.rmi.RemoteException;
    public local.bb.lab.interfaces.Currency[] getCurrencies() throws java.rmi.RemoteException;
    public local.bb.lab.interfaces.Currency getCurrency() throws java.rmi.RemoteException;
}
