package local.bb.lab.servlets;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import local.bb.lab.DaysCounterService;
import local.bb.lab.DaysCounterServiceLocator;
import local.bb.lab.interfaces.Counter;

public class CounterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String days = "unknown";
		try {
			DaysCounterService service = new DaysCounterServiceLocator();
			Counter counter = service.getDaysCounterPort();
			days = counter.getHolidaysInterval()+"";
		} catch (ServiceException | RemoteException e) {
			e.printStackTrace();
		}
		response.getWriter().append("Days untill holidays: " + days);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
