package local.bb.lab.servlets;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import local.bb.lab.PolishExchangeService;
import local.bb.lab.PolishExchangeServiceLocator;
import local.bb.lab.interfaces.Currency;
import local.bb.lab.interfaces.Exchange;

public class ExchangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Currency> currencies = null;
		try {
			PolishExchangeService service = new PolishExchangeServiceLocator();
			Exchange exchange = service.getPolishExchangePort();
			currencies = (List<Currency>)Arrays.asList(exchange.getCurrencies());
		} catch (ServiceException | RemoteException e) {
			e.printStackTrace();
		}
		request.setAttribute("currencies", currencies);
		//attribute
		request.getRequestDispatcher("/exchange.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Currency currency = Currency.fromString(request.getParameter("currency"));
			Double value = Double.parseDouble(request.getParameter("value"));
			System.out.println(currency + "/" + value);
			try {
				PolishExchangeService service = new PolishExchangeServiceLocator();
				Exchange exchange = service.getPolishExchangePort();
				request.setAttribute("result", value + " " + currency + " -> " + 
						exchange.getCurrency() + " = " + exchange.count(currency, value));
			} catch (ServiceException | RemoteException e) {
				e.printStackTrace();
			}
		} catch (NullPointerException | NumberFormatException e) {
		}
		doGet(request, response);
	}

}
