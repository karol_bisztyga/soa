/**
 * PolishExchangeService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package local.bb.lab;

public interface PolishExchangeService extends javax.xml.rpc.Service {
    public java.lang.String getPolishExchangePortAddress();

    public local.bb.lab.interfaces.Exchange getPolishExchangePort() throws javax.xml.rpc.ServiceException;

    public local.bb.lab.interfaces.Exchange getPolishExchangePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
