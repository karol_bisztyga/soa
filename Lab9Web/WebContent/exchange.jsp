<%@page import="local.bb.lab.interfaces.Currency"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>exchange</title>
</head>
<body>

	<form method="post" action="">
		<input type="number" name="value" />
		<select name="currency">
			<%	
				List<Currency> currencies = (List<Currency>)request.getAttribute("currencies");
				if(currencies != null) {
					for(int i=0 ; i<currencies.size() ; ++i) { %>
						<option value="<%= currencies.get(i) %>"><%= currencies.get(i) %></option>
					<% }
				}
			%>
		</select><br />
		<input type="submit" value="count" />
	</form>
	<hr>
	<%
		if(request.getAttribute("result") != null) { %>
		<%= request.getAttribute("result") %>
		<% }
	%>

</body>
</html>