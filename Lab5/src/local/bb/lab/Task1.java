package local.bb.lab;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.management.modelmbean.XMLParseException;
import javax.xml.bind.JAXBException;

import local.bb.lab.Book.Title;

public class Task1 {

	public static void main(String[] args) {
		
		BookManager bm;
		try {
			bm = new BookManager();
			bm.init();
		} catch (IOException | JAXBException | XMLParseException e1) {
			e1.printStackTrace();
			System.err.println("failed to initialize book manager, exiting");
			return;
		}
		System.out.println(bm.getBooks().size());
		Scanner scanner = new Scanner(System.in);
		Question q1 = new Question("would you like to create new book?", new String[]{"y","n"}, scanner);
		bm.printFormatedData();
		while(true) {
			boolean addBook = false;
			q1.ask();
			if(!q1.getAnswer().equals("y")) {
				break;
			}
			
			Book book = new Book();
			Task1 task = new Task1();
			
			task.addAuthor(scanner, book);
			task.addTitle(scanner, book);
			Question isbnQuestion = new Question("isbn: ", null, scanner);
			isbnQuestion.ask();
			book.setIsbn(isbnQuestion.getAnswer());
			
			String action = "";
			Question actionQuestion = new Question(
					"what do you want to do?\n[adda] add author\n[addt] add title\n"
					+ "[done] save this book\n[cancel] abbadon saving this book", 
					new String[]{"adda","addt","done","cancel"}, 
					scanner);
			while(true) {
				actionQuestion.ask();
				action = actionQuestion.getAnswer();
				if(action.equals("adda")) {
					task.addAuthor(scanner, book);
				} else if(action.equals("addt")) {
					task.addTitle(scanner, book);
				} else {
					if(action.equals("done")) {
						addBook = true;
					}
					break;
				}
			}
			if(addBook) {
				System.out.println("saving book: " + book.toString());
				try {
					bm.addBook(book);
				} catch (IOException | JAXBException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("GOODBYE");
	}
	
	public void addAuthor(Scanner scanner, Book book) {
		Question q = new Question("author name: ", null, scanner);
		q.ask();
		book.getAuthors().add(q.getAnswer());
	}
	
	public void addTitle(Scanner scanner, Book book) {
		Question languageQuestion = new Question("title language: ", null, scanner);
		languageQuestion.ask();
		Question titleQuestion = new Question("title in this language: ", null, scanner);
		titleQuestion.ask();
		book.getTitles().add(new Title(languageQuestion.getAnswer(), titleQuestion.getAnswer()));
	}
	
	public static class Question {
		private final String question;
		private final ArrayList<String> possibleAnwers;
		private final Scanner scanner;
		private String answer;
		public Question(String question, String[] answers, Scanner scanner) {
			this.question = question;
			this.scanner = scanner;
			this.answer = "";
			if(answers != null) {
				this.possibleAnwers = new ArrayList<>();
				for(String s : answers) {
					this.possibleAnwers.add(s);
				}
			} else {
				possibleAnwers = null;
			}
		}
		public void ask() {
			String out = this.question;
			if(possibleAnwers != null) {
				out += getPossibleAnswers();
			}
			System.out.println(out);
			while(true) {
				answer = scanner.nextLine();
				if(possibleAnwers!=null && !possibleAnwers.contains(answer) || (possibleAnwers==null && answer.length()==0)) {
					if(possibleAnwers != null) {
						System.out.println("Invalid answer, possible answers: " + getPossibleAnswers());
					} else {
						System.out.println("no data received");
					}
				} else {
					break;
				}
			}
		}
		private String getPossibleAnswers() {
			String res = "[";
			for(String s : possibleAnwers) {
				res += s + ",";
			}
			res = res.substring(0, res.length()-1);
			res += "]";
			return res;
		}
		public String getAnswer() {
			return answer;
		}
	}
}
