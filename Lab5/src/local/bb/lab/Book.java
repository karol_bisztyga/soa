package local.bb.lab;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Book {

	@XmlElementWrapper
	@XmlElement(name="author")
	private final ArrayList<String> authors;
	private String isbn;
	@XmlElementWrapper(name="title")
	private final ArrayList<Title> titles;
	
	public Book() {
		this.authors = new ArrayList<>();
		this.titles = new ArrayList<>();
		this.isbn = "";
	}
	@XmlElement
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public ArrayList<String> getAuthors() {
		return authors;
	}
	public ArrayList<Title> getTitles() {
		return titles;
	} 
	@Override
	public String toString() {
		String authorsString = "";
		for(String a : authors) {
			authorsString += a+",";
		}
		authorsString = authorsString.substring(0, authorsString.length()-1);
		String titlesString = "";
		for(Title t : titles) {
			titlesString += t.toString()+",";
		}
		titlesString = titlesString.substring(0, titlesString.length()-1);
		return "Book [authors=" + authorsString + ", isbn=" + isbn + ", titles=" + titlesString + "]";
	}
	
	
	@XmlRootElement
	public static class Title {

		private String language;
		private String title;
		public Title() {
			this.language = "";
			this.title = "";
		}
		public Title(String language, String title) {
			this.language = language;
			this.title = title;
		}
		public String getLanguage() {
			return language;
		}
		@XmlElement
		public void setLanguage(String language) {
			this.language = language;
		}
		@XmlElement
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		@Override
		public String toString() {
			return "Title [language=" + language + ", title=" + title + "]";
		}
	}
}
