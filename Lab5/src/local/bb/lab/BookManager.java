package local.bb.lab;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.management.modelmbean.XMLParseException;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

@XmlRootElement(namespace="local.bb.lab")
//@XmlSeeAlso({Book.class, Book.Title.class})
public class BookManager {

	private static final String FILENAME = "books.xml";
	private static final String VALIDATORNAME = "booksValidator.xsd";

	@XmlElementWrapper
	@XmlElement(name="book")
	private final ArrayList<Book> books = new ArrayList<>();
	
	private JAXBContext ctx;
	
	public BookManager() {
	}
	
	public void init() throws JAXBException, IOException, XMLParseException {
		File file = new File(FILENAME);
		ctx = JAXBContext.newInstance(BookManager.class);
		if(file.exists()) {
			Unmarshaller jaxbUnmarshaller = ctx.createUnmarshaller();
			BookManager other = (BookManager) jaxbUnmarshaller.unmarshal(file);
			copyData(other);
			
			if(!validateXMLSchema(VALIDATORNAME, FILENAME)) {
				throw new XMLParseException("invalid xml");
			};
			
		}
	}
	
	public void printFormatedData() {
		for(Book b : books) {
			for(Book.Title t : b.getTitles()) {
				System.out.println("Title in language: {" + t.getLanguage() + "}: {" + t.getTitle() + "}");
			}
			String authorsString = "Authors: ";
			for(String a : b.getAuthors()) {
				authorsString += "{"+ a +"};";
			}
			authorsString = authorsString.substring(0, authorsString.length()-1);
			System.out.println(authorsString);
			System.out.println("ISBN: " + b.getIsbn());
			System.out.println();
		}
	}
	
	private void copyData(BookManager from) throws IOException, JAXBException {
		for(Book b : from.getBooks()) {
			this.addBook(b);
		}
	}
	
	public void addBook(Book book) throws IOException, JAXBException {
		this.books.add(book);
		this.saveBooks();
	}
	
	public void saveBooks() throws IOException, JAXBException {
		File file = new File(FILENAME);
		file.createNewFile();
		ctx = JAXBContext.newInstance(BookManager.class);
		Marshaller m = ctx.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(this, file);
	}

	private boolean validateXMLSchema(String xsdPath, String xmlPath) {
        
        try {
            SchemaFactory factory = 
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (IOException | SAXException e) {
            System.out.println("Exception: "+e.getMessage());
            return false;
        }
        return true;
    }

	public ArrayList<Book> getBooks() {
		return books;
	}
	
}
