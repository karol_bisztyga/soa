<%@page import="local.bb.lab.entities.Book"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>editing</title>
</head>
<body>
	<h1>editing book:</h1>
	<a href="home">home</a>
	<% 
		Book book = (Book)request.getAttribute("book");
		if(book == null) { %>
			<span style="color: red;">no such book found :(</span>
		<% } else { %>
	<form method="post" action="">
		<table>
			<tr>
				<td>author first name</td>
				<td>
					<input type="text" name="first" required value="<%= book.getAuthorFirstName() %>" />
				</td>
			</tr>
			<tr>
				<td>author last name</td>
				<td>
					<input type="text" name="last" required value="<%= book.getAuthorSecondName() %>" />
				</td>
			</tr>
			<tr>
				<td>title</td>
				<td>
					<input type="text" name="title" required value="<%= book.getTitle() %>" />
				</td>
			</tr>
			<tr>
				<td>year</td>
				<td>
					<input type="number" name="year" required value="<%= book.getYear() %>" />
				</td>
			</tr>
			<tr>
				<td>ISBN</td>
				<td>
					<input type="number" name="isbn" required value="<%= book.getISBN() %>" />
				</td>
			</tr>
			<tr>
				<td>price</td>
				<td>
					<input type="number" name="price" required value="<%= book.getPrice() %>" />
				</td>
			</tr>
		</table>
		<input type="submit" value="save" />
	</form>
		<% } %>
</body>
</html>