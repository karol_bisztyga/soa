<%@page import="local.bb.lab.entities.Book"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>books</title>
</head>
<body>

	<table border="3">
		<tr>
			<td>author</td>
			<td>title</td>
			<td>year</td>
			<td>ISBN</td>
			<td>price</td>
			<td>edit</td>
			<td>remove</td>
		</tr>
		<% 
			ArrayList<Book> books = (ArrayList<Book>)request.getAttribute("books");
			for(Book book : books) {
		%>
				<tr>
					<td><%= book.getAuthorFirstName() %> <%= book.getAuthorSecondName() %></td>
					<td><%= book.getTitle() %></td>
					<td><%= book.getYear() %></td>
					<td><%= book.getISBN() %></td>
					<td><%= book.getPrice() %></td>
					<td>
						<a href="edit?id=<%= book.getId() %>">edit</a>
					</td>
					<td>
					<form method="post" action="">
						<input type="text" style="display: none;" name="action" value="remove" />
						<input type="text" style="display: none;" name="id" value="<%= book.getId() %>" />
						<input type="submit" value="remove" />
					</form>
					</td>
				</tr>
		<% } %>
	</table>
	<hr />
	<form method="post" action="">
	<input type="text" name="action" value="add" style="display: none;">
		<table>
			<tr>
				<td>author first name</td>
				<td>
					<input type="text" name="first" required />
				</td>
			</tr>
			<tr>
				<td>author last name</td>
				<td>
					<input type="text" name="last" required />
				</td>
			</tr>
			<tr>
				<td>title</td>
				<td>
					<input type="text" name="title" required />
				</td>
			</tr>
			<tr>
				<td>year</td>
				<td>
					<input type="number" name="year" required />
				</td>
			</tr>
			<tr>
				<td>ISBN</td>
				<td>
					<input type="number" name="isbn" required />
				</td>
			</tr>
			<tr>
				<td>price</td>
				<td>
					<input type="number" name="price" required />
				</td>
			</tr>
		</table>
		<input type="submit" value="add" />
	</form>
	
	
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script>
			/*$(document).ready(function() {
				function click(e) {
					var action = e.target.id.split("-")[0];
					var id = e.target.id.split("-")[1];
					alert("clicked " + action + "," + id);
				}
				
				$(".edit").click(click);
				$(".remove").click(click);
			});*/
		</script>
	
</body>
</html>