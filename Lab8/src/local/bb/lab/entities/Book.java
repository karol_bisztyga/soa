package local.bb.lab.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.transaction.Transactional;

@Entity(name="books")
@Transactional
public class Book {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String authorFirstName;
	private String authorSecondName;
	private String title;
	private String ISBN;
	private int year;
	private double price;
	
	public Book() {}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAuthorFirstName() {
		return authorFirstName;
	}
	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}
	public String getAuthorSecondName() {
		return authorSecondName;
	}
	public void setAuthorSecondName(String authorSecondName) {
		this.authorSecondName = authorSecondName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Book [id=" + id + ", authorFirstName=" + authorFirstName + ", authorSecondName=" + authorSecondName
				+ ", title=" + title + ", ISBN=" + ISBN + ", year=" + year + ", price=" + price + "]";
	}

	public static Book getBook(String authorFirstName, String authorSecondName,	String title, String ISBN, int year, double price) {
		Book book = new Book();
		book.setAuthorFirstName(authorFirstName);
		book.setAuthorSecondName(authorSecondName);
		book.setTitle(title);
		book.setISBN(ISBN);
		book.setYear(year);
		book.setPrice(price);
		return book;
	}
}
