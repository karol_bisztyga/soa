package local.bb.lab;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import local.bb.lab.entities.Book;
import local.bb.lab.tools.DatabaseManager;

@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Book> books = new ArrayList<>();
		try {
			Session session = DatabaseManager.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			books = (ArrayList<Book>)session.createCriteria(Book.class).list();
			tx.commit();
			System.out.println(books.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
		request.setAttribute("books", books);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action.equals("add")) {
			String firstName = request.getParameter("first");
			String lastName = request.getParameter("last");
			String title = request.getParameter("title");
			Integer year = Integer.parseInt(request.getParameter("year"));
			String isbn = request.getParameter("isbn");
			Double price = Double.parseDouble(request.getParameter("price"));
			
			Book book = Book.getBook(firstName, lastName, title, isbn, year, price);
			System.out.println("adding " + book);
			
			try {
				Session session = DatabaseManager.getSessionFactory().openSession();
				Transaction tx = session.beginTransaction();
				tx.begin();
				session.save(book);
				tx.commit();
				System.out.println("added " + book);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if(action.equals("remove")) {
			Integer id = Integer.parseInt(request.getParameter("id"));
			System.out.println("looking for book with id " + id);
			try {
				Session session = DatabaseManager.getSessionFactory().openSession();
				Transaction tx = session.beginTransaction();
				tx.begin();
				Book book = (Book) session.createCriteria(Book.class).add(Restrictions.eq("id", id)).list().get(0);
				System.out.println("found and removing " + book);
				session.delete(book);
				System.out.println("removed " + book);
				tx.commit();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		doGet(request, response);
	}

}
