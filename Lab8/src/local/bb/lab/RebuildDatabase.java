package local.bb.lab;

import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import local.bb.lab.entities.Book;

public class RebuildDatabase {

	public static void main(String[] args) {
		
		try {
			
			Configuration configuration = new Configuration();
			//entities
			configuration.addAnnotatedClass(Book.class);
			//
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));

			configuration.setProperties(properties);

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().
			applySettings(configuration.getProperties()).build(); 

			SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);

			Session session = factory.getCurrentSession();

			session.getTransaction().begin();
			
			session.save(Book.getBook("JK", "Rowling", "Harry Potter and Hibernate", "12343534523", 2020, 9.99));
			session.save(Book.getBook("George", "Lucas", "Star Wars: The Hibernate", "78957464445", 2015, 19.99));
			session.save(Book.getBook("George W", "Bush", "Memories", "23423534534", 2012, 29.99));

			session.getTransaction().commit();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
