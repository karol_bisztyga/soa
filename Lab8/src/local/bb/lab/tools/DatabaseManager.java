package local.bb.lab.tools;

import java.io.IOException;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import local.bb.lab.entities.Book;

public class DatabaseManager {
	
	private static SessionFactory factory;
	
	public static SessionFactory getSessionFactory() throws IOException {
		if(factory == null) {
			Configuration configuration = new Configuration();
			configuration.setProperty("hibernate.temp.use_jdbc_metadata_defaults","false");
			//entities
			configuration.addAnnotatedClass(Book.class);
			//
			
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));

			configuration.setProperties(properties);

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().
			applySettings(configuration.getProperties()).build(); 

			factory = configuration.buildSessionFactory(serviceRegistry);
		}
		return factory;
	}

}
