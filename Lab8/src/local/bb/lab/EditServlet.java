package local.bb.lab;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import local.bb.lab.entities.Book;
import local.bb.lab.tools.DatabaseManager;

@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Book book;
		Integer id = Integer.parseInt(request.getParameter("id"));
		try {
			Session session = DatabaseManager.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			book = (Book) session.createCriteria(Book.class).add(Restrictions.eq("id", id)).list().get(0);
			System.out.println("found and editing " + book);
			tx.commit();
			request.setAttribute("book", book);
		} catch (IOException e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("/edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id = Integer.parseInt(request.getParameter("id"));
		String firstName = request.getParameter("first");
		String lastName = request.getParameter("last");
		String title = request.getParameter("title");
		Integer year = Integer.parseInt(request.getParameter("year"));
		String isbn = request.getParameter("isbn");
		Double price = Double.parseDouble(request.getParameter("price"));
		
		Book book;
		
		try {
			Session session = DatabaseManager.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			book = (Book) session.createCriteria(Book.class).add(Restrictions.eq("id", id)).list().get(0);
			book.setAuthorFirstName(firstName);
			book.setAuthorSecondName(lastName);
			book.setTitle(title);
			book.setYear(year);
			book.setISBN(isbn);
			book.setPrice(price);
			session.update(book);
			tx.commit();
			System.out.println("edited " + book);
		} catch (IOException e) {
			e.printStackTrace();
		}
		response.sendRedirect("home");
	}

}
