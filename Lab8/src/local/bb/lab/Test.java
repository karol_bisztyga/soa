package local.bb.lab;

import java.io.IOException;
import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import local.bb.lab.entities.Book;
import local.bb.lab.tools.DatabaseManager;

public class Test {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		try {
			SessionFactory sf = DatabaseManager.getSessionFactory();
			Session session = sf.openSession();
			session.getTransaction().begin();
			ArrayList<Book> books = (ArrayList<Book>)session.createCriteria(Book.class).list();
			session.getTransaction().commit();
			System.out.println(books.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
