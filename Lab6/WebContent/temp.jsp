<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>temperature converter</title>
</head>
<body>
	<h1>
		temperature converter
	</h1>
	
	<form method="POST" action="">
		<select id="select" name="from">
			<option value="C">C to F</option>
			<option value="F">F to C</option>
		</select>
		 <input type="number" name="value" /><br />
		 <input type="submit" value="convert" />
	</form>
	<br />
	<% if(request.getAttribute("msg") != null && !request.getAttribute("msg").equals("")) { %>
		<%= request.getAttribute("msg") %>
	<% } %>
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script>
		$(document).ready(function(){
			$("#select").change(function() {
				var newVal = ($("#select").val() == 'C') ? 'F' : 'C' ;
				$("#to").html(newVal);
			});
		});
	</script>

</body>
</html>