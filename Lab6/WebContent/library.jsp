<%@page import="local.bb.lab.xml.Book.STATUS"%>
<%@page import="java.util.ArrayList"%>
<%@page import="local.bb.lab.xml.Book"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>library</title>

<style>
	td {
		padding: 3px;
	}
	#tr-head > td {
		background-color: #FF0;
	}
</style>

</head>
<body>
	<h1>library</h1>
	
	<table border="1">
		<tr id="tr-head">
			<td>Author</td>
			<td>Title</td>
			<td>Status</td>
			<td>actions</td>
		</tr>
		<% 
		ArrayList<Book> books = (ArrayList<Book>)request.getAttribute("books");
		for(Book b : books) { %>
			<tr>
				<td><%= b.getTitle() %></td>
				<td><%= b.getAuthor() %></td>
				<td><%= b.getStatus() %></td>
				<td>
					<% if(b.getStatus() == STATUS.FREE) { %>
						<input type="button" class="libraryRequestBtn" value="reservation" id="reservation-<%= b.getId() %>" />
						<input type="button" class="libraryRequestBtn" value="borrow" id="borrow-<%= b.getId() %>" />
					<% } else if(b.getStatus() == STATUS.RESERVED) { %>
						<input type="button" class="libraryRequestBtn" value="borrow" id="borrow-<%= b.getId() %>" />
						<input type="button" class="libraryRequestBtn" value="cancel reservation" id="cancelReservation-<%= b.getId() %>" />
					<% } else { %>
						<input type="button" class="libraryRequestBtn" value="return" id="return-<%= b.getId() %>" />
					<% } %>
				</td>
			</tr>
		<% } %>
	</table>

	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script>
		$(document).ready(function(){
			
			$(".libraryRequestBtn").click(function() {
				var action = $(this).attr('id').split("-")[0];
				var id = $(this).attr('id').split("-")[1];
				$.post("library", {action: action, id: id}, function(result){
					if(result != 'ok') {
						alert(result);
					} else {
						location.reload();
					}
			    });
			});
			
		});
	</script>
</body>
</html>