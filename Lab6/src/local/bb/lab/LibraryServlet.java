package local.bb.lab;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import local.bb.lab.ejb.Library;
import local.bb.lab.exception.LibraryException;
import local.bb.lab.xml.Book;
import local.bb.lab.xml.Book.STATUS;

public class LibraryServlet extends HttpServlet {
	
	@EJB
	public Library library;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("books", library.getBookManager().getBooks());
		request.getRequestDispatcher("/library.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		Integer id = Integer.parseInt(request.getParameter("id"));
		String result = "ok";
		try {
			Book b = library.getBookManager().getBookById(id);
			if(action.equals("borrow")) {
				if(b.getStatus() == STATUS.TAKEN) {
					throw new LibraryException();
				}
				b.setStatus(STATUS.TAKEN);
			} else if(action.equals("reservation")) {
				if(b.getStatus() != STATUS.FREE) {
					throw new LibraryException();
				}
				b.setStatus(STATUS.RESERVED);
			} else if(action.equals("cancelReservation")) {
				if(b.getStatus() != STATUS.RESERVED) {
					throw new LibraryException();
				}
				b.setStatus(STATUS.FREE);
			} else if(action.equals("return")) {
				if(b.getStatus() != STATUS.TAKEN) {
					throw new LibraryException();
				}
				b.setStatus(STATUS.FREE);
			}
			library.getBookManager().saveBooks();
		} catch (NullPointerException e) {
			result = "error, no such book";
		} catch(LibraryException e) {
			result = "could not perform this action";
		} catch (JAXBException e) {
			result = "error";
			e.printStackTrace();
		}
	    response.getWriter().print(result);
	    response.getWriter().flush();
	}

}
