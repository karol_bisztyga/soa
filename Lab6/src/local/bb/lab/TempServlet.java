package local.bb.lab;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.ejb.Converter;


public class TempServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	Converter converter;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/temp.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg = "";
		try {
			Double value = Double.parseDouble(request.getParameter("value"));
			Character from = request.getParameter("from").charAt(0);
			char to = (from == 'C') ? 'F' : 'C' ;
			double res = (from == 'C') ? converter.C2F(value) : converter.F2C(value) ;
			msg = "converted(" + from + " to " + to + ") "  + value + " = " + res; 
		} catch (Exception e) {
			e.printStackTrace();
			msg = "error";
		}
		System.out.println("msg: " + msg);
		request.setAttribute("msg", msg);
		request.getRequestDispatcher("/temp.jsp").forward(request, response);
	}

}
