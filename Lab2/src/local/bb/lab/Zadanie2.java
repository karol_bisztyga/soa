package local.bb.lab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.model.GameModel;
import local.bb.lab.model.GameModel.RESULT;

public class Zadanie2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public Zadanie2() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/game.html").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = "none";
		GameModel gameModel = new GameModel();
		String p1 = request.getParameter("one");
		String p2 = request.getParameter("two");
		try {
			GameModel.RESULT result = gameModel.getWinner(p1, p2);
			if(result == RESULT.DRAW) message = "it's a draw!";
			else {
				message = "the winner is... player " + result.toString();
			}
		} catch (Exception e) {
			message = e.getMessage();
		}
		request.getRequestDispatcher("/response.jsp?msg="+message).forward(request, response);
	
	}

}
