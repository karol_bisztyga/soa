package local.bb.lab.model;

public class GameModel {
	
	public enum RESULT {
		ONE,TWO,DRAW
	}
	
	public RESULT getWinner(String p1, String p2) throws Exception {
		if(p1.equals("paper")) {
			if(p2.equals("paper")) return RESULT.DRAW;
			if(p2.equals("rock")) return RESULT.ONE;
			if(p2.equals("scissors")) return RESULT.TWO;
		}
		if(p1.equals("rock")) {
			if(p2.equals("paper")) return RESULT.TWO;
			if(p2.equals("rock")) return RESULT.DRAW;
			if(p2.equals("scissors")) return RESULT.ONE;
		}
		if(p1.equals("scissors")) {
			if(p2.equals("paper")) return RESULT.ONE;
			if(p2.equals("rock")) return RESULT.TWO;
			if(p2.equals("scissors")) return RESULT.DRAW;
		}
		throw new Exception("unknown data");
	}
}
