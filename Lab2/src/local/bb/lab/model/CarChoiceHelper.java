package local.bb.lab.model;

import java.util.ArrayList;

import local.bb.lab.model.CarChoiceHelper.Car.TYPE;

public class CarChoiceHelper {
	
	private ArrayList<Car> cars = new ArrayList<>();
	private static CarChoiceHelper instance;
	
	private CarChoiceHelper() {
		cars.add(new Car(TYPE.CITY,"smart",10000));
		cars.add(new Car(TYPE.CITY,"toyota yaris",40000));
		cars.add(new Car(TYPE.CITY,"mini buggati",20000));

		cars.add(new Car(TYPE.SPORT,"ford focus",30000));
		cars.add(new Car(TYPE.SPORT,"bmw",50000));
		cars.add(new Car(TYPE.SPORT,"rolls royce",300000));

		cars.add(new Car(TYPE.LUXURY,"mercedes",200000));
		cars.add(new Car(TYPE.LUXURY,"lamborgini",500000));
		cars.add(new Car(TYPE.LUXURY,"porshe",900000));
	}
	
	public ArrayList<Car> getSuitableCars(TYPE type, int minPrice, int maxPrice) {
		ArrayList<Car> result = new ArrayList<>();
		for(Car car : cars) {
			if(type == car.getType() && car.getPrice() >= minPrice &&
					car.getPrice() <= maxPrice) {
				result.add(car);
			}
		}
		return (result.size() > 0) ? result : null;
	}
	
	public static CarChoiceHelper getInstance() {
		if(instance == null) {
			instance = new CarChoiceHelper();
		}
		return instance;
	}
	
	public static class Car {
		public enum TYPE {
			CITY,
			SPORT,
			LUXURY
		}
		
		private final TYPE type;
		private final String name;
		private final int price;
		
		public Car(TYPE type, String name, int price) {
			this.type = type;
			this.name = name;
			this.price = price;
		}

		public TYPE getType() {
			return type;
		}

		public String getName() {
			return name;
		}

		public int getPrice() {
			return price;
		}
	}
}
