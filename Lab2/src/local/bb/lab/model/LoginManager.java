package local.bb.lab.model;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginManager {

	private final String login = "asd";
	private final String password = "asd";
	
	private static LoginManager instance;
	
	private LoginManager() {}
	
	public static LoginManager getInstance() {
		if(instance == null) {
			instance = new LoginManager();
		}
		return instance;
	}
	
	public boolean loggedIn(HttpServletRequest request) {
		try {
			Cookie[] cookies = request.getCookies();
			for(Cookie c : cookies) {
				if(c.getName().equals("login")) {
					return true;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	public boolean login(String login, String password, HttpServletResponse response) {
		Cookie cookie = new Cookie("login", login);
		if(this.login.equals(login) && this.password.equals(password)) {
			response.addCookie(cookie);
			return true;
		}
		cookie.setMaxAge(0);
		response.addCookie(cookie);
		return false;
	}

	public String getLogin() {
		return login;
	}
	
	

}
