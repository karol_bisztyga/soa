package local.bb.lab.model;

import java.util.Date;
import java.util.Vector;

public class GuestBookManager {
	
	private static GuestBookManager instance;
	private Vector<GuestBookEntry> entries = new Vector<>();
	private GuestBookManager() {}
	
	public static GuestBookManager getInstance() {
		if(instance == null) {
			instance = new GuestBookManager();
		}
		return instance;
	}
	
	public synchronized Vector<GuestBookEntry> getEntries() {
		return entries;
	}
	
	public synchronized void addEntry(GuestBookEntry entry) {
		this.entries.addElement(entry);
	}

	public static class GuestBookEntry {
		private final Date date;
		private final String name;
		private final String text;
		public GuestBookEntry(Date date, String name, String text) {
			this.date = date;
			this.name = name;
			this.text = text;
		}
		public Date getDate() {
			return date;
		}
		public String getName() {
			return name;
		}
		public String getText() {
			return text;
		}
	}

}
