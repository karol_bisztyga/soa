package local.bb.lab;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.model.CarChoiceHelper;
import local.bb.lab.model.CarChoiceHelper.Car;
import local.bb.lab.model.CarChoiceHelper.Car.TYPE;

public class CarChoiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		request.getRequestDispatcher("/cars.html").forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String message = "";
		String typeString = request.getParameter("type");
		TYPE type = TYPE.CITY;
		if(typeString.equals("sport")) type = TYPE.SPORT;
		else if(typeString.equals("city")) type = TYPE.CITY;
		else if(typeString.equals("luxury")) type = TYPE.LUXURY;
		Integer priceMin = Integer.parseInt(request.getParameter("price-from"));
		Integer priceMax = Integer.parseInt(request.getParameter("price-to"));
		ArrayList<Car> cars = CarChoiceHelper.getInstance().getSuitableCars(type, priceMin, priceMax);
		if(cars == null) {
			message = "no cars found";
		} else {
			for(Car c : cars) {
				message += "-"+ c.getName() +", price: "+c.getPrice()+"$<br />";
			}
		}
		request.getRequestDispatcher("/response.jsp?msg="+message).forward(request, response);
	}

}
