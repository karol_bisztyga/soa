package local.bb.lab;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import local.bb.lab.model.GuestBookManager;
import local.bb.lab.model.LoginManager;

public class GuestBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(!LoginManager.getInstance().loggedIn(request)) {
			response.sendRedirect("/Lab2/login");
			return;
		}
		request.getRequestDispatcher("/guestbook.html").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		response.setContentType("application/json");
		Gson gson = new Gson();
		String jsonData = "action " + action;
		System.out.println(jsonData);
		if(action.equals("put")) {
			String text = request.getParameter("text");
			String name = request.getParameter("name");
			if(text.length() > 0 && name.length() > 0) {
				GuestBookManager.getInstance().addEntry(
						new GuestBookManager.GuestBookEntry(new Date(), name , text));
			}
		}
		jsonData = gson.toJson(GuestBookManager.getInstance().getEntries());
		PrintWriter out = response.getWriter();
		out.print(jsonData);
		out.flush();
	}

}
