package local.bb.lab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Zadanie1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/forma1.html").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = "none";
		String name;
		Integer age;
		try {
			age = Integer.parseInt(request.getParameter("age"));
			name = request.getParameter("name");
			
			if(name.substring(name.length()-1, name.length()).equals("a")) {
				message = "you're a woman";
			} else {
				message = "you're a man";
			}
			message += " and ";
			if(age > 18) {
				message += " you're an adult";
			} else {
				message += "you're still a kid";
			}
		} catch (NullPointerException e) {
			message = "you have to provide name and age";
		}
		request.getRequestDispatcher("/response.jsp?msg="+message).forward(request, response);
	}

}
