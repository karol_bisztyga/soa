package local.bb.lab;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Zadanie3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public Zadanie3() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("numbers");
		try {
			String[] numbers = message.split(",");
			if(numbers.length != 5) {
				throw new Exception("you must pass 5 numbers with splitter ','");
			}
			double sum = 0;
			for(int i=0 ; i<numbers.length ; ++i) {
				Double num = Double.parseDouble(numbers[i]);
				sum += num;
			}
			double avg = sum/5;
			message = "average value = " + avg;
		} catch(NullPointerException e) {
			request.getRequestDispatcher("/numbers.html").forward(request, response);
			return;
		} catch (Exception e) {
			message = e.getMessage();
		}
		request.getRequestDispatcher("/response.jsp?msg="+message).forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Integer> numbers = new ArrayList<>();
		int counter = 0;
		while(true) {
			try {
				Integer num = Integer.parseInt(request.getParameter("value"+counter));
				numbers.add(num);
				System.out.println("num: " + num);
				++counter;
			} catch (Exception e) {
				break;
			}
		}
		numbers.sort(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				if(o1>o2)return 1;
				else if(01<o2) return -1;
				return 0;
			}
		});
		String message = "[";
		for(int i=0 ; i<numbers.size() ; ++i) {
			message += numbers.get(i)+",";
		}
		message = message.substring(0, message.length()-1);
		message += "]";
		request.getRequestDispatcher("/response.jsp?msg="+message).forward(request, response);
		
	}

}
