package local.bb.lab;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import local.bb.lab.model.LoginManager;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg = request.getParameter("msg");
		String redirect = (msg == null) ? "/login.jsp" : "/login.jsp?msg="+msg ;
		request.getRequestDispatcher(redirect).forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg = "invalid data";
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		try {
			if(login.equals("")) {
				throw new Exception("no login received");
			}
			if(password.equals("")) {
				throw new Exception("no password received");
			}
		} catch (Exception e) {
			msg = e.getMessage();
		}
		String redirect = (LoginManager.getInstance().login(login, password, response)) ? "index.html" : "/login.jsp?msg="+msg ;
		request.getRequestDispatcher(redirect).forward(request, response);
	}

}
