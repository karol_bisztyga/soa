package local.bb.lab.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.management.modelmbean.XMLParseException;
import javax.xml.bind.JAXBException;

public class Task1 {

	public static void main(String[] args) {
		
		BookManager bm;
		try {
			bm = new BookManager();
			bm.init();
		} catch (IOException | JAXBException | XMLParseException e1) {
			e1.printStackTrace();
			System.err.println("failed to initialize book manager, exiting");
			return;
		}
		System.out.println("there are " + bm.getBooks().size() + " books in the library");
		Scanner scanner = new Scanner(System.in);
		
		for(Book b : bm.getBooks()) {
			System.out.println(b);
		}
		
		Question q1 = new Question("would you like to create new book?", new String[]{"y","n"}, scanner);
		
		while(true) {
			q1.ask();
			if(!q1.getAnswer().equals("y")) {
				break;
			}
			
			Book book = new Book();

			Question authorQ = new Question("author: ", null, scanner);
			authorQ.ask();
			Question titleQ = new Question("title: ", null, scanner);
			titleQ.ask();
			
			book.setTitle(titleQ.getAnswer());
			book.setAuthor(authorQ.getAnswer());

			System.out.println("saving book: " + book.toString());
			try {
				bm.addBook(book);
			} catch (IOException | JAXBException e) {
				e.printStackTrace();
			}
		}
		System.out.println("GOODBYE");
	}
	
	public static class Question {
		private final String question;
		private final ArrayList<String> possibleAnwers;
		private final Scanner scanner;
		private String answer;
		public Question(String question, String[] answers, Scanner scanner) {
			this.question = question;
			this.scanner = scanner;
			this.answer = "";
			if(answers != null) {
				this.possibleAnwers = new ArrayList<>();
				for(String s : answers) {
					this.possibleAnwers.add(s);
				}
			} else {
				possibleAnwers = null;
			}
		}
		public void ask() {
			String out = this.question;
			if(possibleAnwers != null) {
				out += getPossibleAnswers();
			}
			System.out.println(out);
			while(true) {
				answer = scanner.nextLine();
				if(possibleAnwers!=null && !possibleAnwers.contains(answer) || (possibleAnwers==null && answer.length()==0)) {
					if(possibleAnwers != null) {
						System.out.println("Invalid answer, possible answers: " + getPossibleAnswers());
					} else {
						System.out.println("no data received");
					}
				} else {
					break;
				}
			}
		}
		private String getPossibleAnswers() {
			String res = "[";
			for(String s : possibleAnwers) {
				res += s + ",";
			}
			res = res.substring(0, res.length()-1);
			res += "]";
			return res;
		}
		public String getAnswer() {
			return answer;
		}
	}
}
