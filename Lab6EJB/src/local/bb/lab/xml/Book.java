package local.bb.lab.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Book {
	
	public enum STATUS {
		FREE,
		RESERVED,
		TAKEN
	}

	private static int currentId = 0;
	
	private final int id = ++currentId;
	private String author;
	private String title;
	private STATUS status;
	
	public Book() {
		this("","");
	}
	
	public Book(String author, String title) {
		this.author = author;
		this.title = title;
		this.status = STATUS.FREE;
	}
	@XmlElement
	public STATUS getStatus() {
		return status;
	}
	public void setStatus(STATUS status) {
		this.status = status;
	}
	@XmlElement
	public String getAuthor() {
		return author;
	}
	@XmlElement
	public String getTitle() {
		return title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", author=" + author + ", title=" + title + ", status=" + status + "]";
	}
	
}
