package local.bb.lab.ejb;

import java.io.IOException;

import javax.ejb.Stateless;
import javax.management.modelmbean.XMLParseException;
import javax.xml.bind.JAXBException;

import local.bb.lab.xml.BookManager;

@Stateless
public class Library {
	
	private final BookManager bookManager;

	public Library() {
		bookManager = new BookManager();
		try {
			bookManager.init();
		} catch (JAXBException | IOException | XMLParseException e) {
			e.printStackTrace();
		}
	}
	public BookManager getBookManager() {
		return bookManager;
	}
	
}
