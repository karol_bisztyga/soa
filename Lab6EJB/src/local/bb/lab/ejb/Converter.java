package local.bb.lab.ejb;

import javax.ejb.Stateless;

@Stateless
public class Converter {
	
	public double F2C(double f) {
		return 5.0/9.0*(f-32.0);
	}
	
	public double C2F(double c) {
		return 9.0/5.0*c+32.0;
	}
	
}
