package local.bb.lab.rest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.google.gson.Gson;

import local.bb.lab.persistance.Cat;
import local.bb.lab.persistance.Cats;

@Path("/cats")
public class CatsService {
	
	Gson gson = new Gson();
	
	@GET
	@Path("/")
	public String getCats() {
		return gson.toJson(Cats.getInstance().getCats());
	}
	
	@GET
	@Path("/{id}")
	public String getCat(@PathParam("id") Integer id) {
		Cat result = null;
		for(Cat cat : Cats.getInstance().getCats()) {
			if(cat.getId() == id) {
				result = cat;
				break;
			}
		}
		return gson.toJson(result);
	}
	
	@POST
	@Path("/")
	public void createNewList() {
		Cats.getInstance().getCats().clear();
	}
	
	@PUT
	@Path("/{name}/{age}")
	public void addCat(@PathParam("name") String name, @PathParam("age") Integer age) {
		Cats.getInstance().getCats().add(new Cat(name, age));
	}
	
	@DELETE
	@Path("/{id}")
	public void removeCat(@PathParam("id") Integer id) {
		for(int i=0 ; i<Cats.getInstance().getCats().size() ; ++i) {
			if(Cats.getInstance().getCats().get(i).getId() == id) {
				Cats.getInstance().getCats().remove(i);
				return;
			}
		}
	}
	
}
