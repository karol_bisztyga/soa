package local.bb.lab.persistance;

import java.util.ArrayList;
import java.util.List;

public class Cats {
	
	private final List<Cat> cats;
	private static Cats instance;
	
	private Cats() {
		cats = new ArrayList<>();
		cats.add(new Cat("tom", 5));
		cats.add(new Cat("qwe", 2));
		cats.add(new Cat("asd", 9));
		cats.add(new Cat("ghj", 1));
	}
	
	public static Cats getInstance() {
		if(instance == null) {
			instance = new Cats();
		}
		return instance;
	}

	public List<Cat> getCats() {
		return cats;
	}
	
}
