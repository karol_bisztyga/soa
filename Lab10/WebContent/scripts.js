$(document).ready(function() {
	
	getCollection();
	
	$("#refresh-list").click(getCollection);

	function getCollection() {
		$.ajax({
		    url: 'rest/cats',
		    type: 'GET',
		    success: function(result) {
		    	list = JSON.parse(result);
		    	console.log(list);
	    		$("#list").empty();
		    	for(var i=0 ; i<list.length ; ++i) {
		    		var item = list[i];
		    		var li = $(document.createElement("li"));
		    		var span = $(document.createElement("span"));
		    		span.html(item['name']);
		    		var btnDetails = $(document.createElement("input"));
		    		btnDetails.attr("type", "button");
		    		btnDetails.attr("value", "details");
		    		btnDetails.attr("id", "details-" + item['id']);
		    		btnDetails.click(function(e) {
		    			var id = e.target.id.split("-")[1];
		    			console.log("details " + id);
		    			$.ajax({
		    			    url: 'rest/cats/'+id,
		    			    type: 'GET',
		    			    success: function(result) {
		    			    	console.log(result);
		    			    	cat = JSON.parse(result);
		    			    	$("#details").html("name: " + cat['name'] + "<br />" +
		    			    			"age: " + cat['age']);
		    			    }
		    			});
		    		});
		    		
		    		var btnRemove = $(document.createElement("input"));
		    		btnRemove.attr("type", "button");
		    		btnRemove.attr("value", "remove");
		    		btnRemove.attr("id", "remove-" + item['id']);
		    		btnRemove.click(function(e) {
		    			var id = e.target.id.split("-")[1];
		    			console.log("remove " + id);
		    			$.ajax({
		    			    url: 'rest/cats/'+id,
		    			    type: 'DELETE',
		    			    success: function(result) {
		    			    	getCollection();
		    			    }
		    			});
		    		});
		    		
		    		li.append(span);
		    		li.append(btnDetails);
		    		li.append(btnRemove);
		    		$("#list").append(li);
		    	}
		    }
		});
	}
	
	$("#create").click(function() {
		$.ajax({
		    url: 'rest/cats',
		    type: 'POST',
		    success: function(result) {
		    	getCollection();
		    }
		});
	});
	/*
	$(".remove").click(function(e) {
		var id = e.target.id.split("-")[1];
		$.ajax({
		    url: 'rest/cats/'+id,
		    type: 'DELETE',
		    success: function(result) {
		    	getCollection();
		    }
		});
	});
	*/
	$("#add").click(function(e) {
		var name = $("#name").val();
		var age = $("#age").val();
		if(name == null || name == "", age == null || age == "") {
			return;
		}
		$.ajax({
		    url: 'rest/cats/'+name+"/"+age,
		    type: 'PUT',
		    success: function(result) {
		    	getCollection();
		    }
		});
	});
	
});