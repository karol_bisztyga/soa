package local.bb.lab.soap.interfaces;

public class PlacesInterfaceProxy implements local.bb.lab.soap.interfaces.PlacesInterface {
  private String _endpoint = null;
  private local.bb.lab.soap.interfaces.PlacesInterface placesInterface = null;
  
  public PlacesInterfaceProxy() {
    _initPlacesInterfaceProxy();
  }
  
  public PlacesInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initPlacesInterfaceProxy();
  }
  
  private void _initPlacesInterfaceProxy() {
    try {
      placesInterface = (new local.bb.lab.soap.services.PlacesServiceServiceLocator()).getPlacesServicePort();
      if (placesInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)placesInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)placesInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (placesInterface != null)
      ((javax.xml.rpc.Stub)placesInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public local.bb.lab.soap.interfaces.PlacesInterface getPlacesInterface() {
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    return placesInterface;
  }
  
  public void stopSecurityThread(java.lang.String arg0) throws java.rmi.RemoteException, local.bb.lab.soap.interfaces.PlaceException{
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    placesInterface.stopSecurityThread(arg0);
  }
  
  public void startSecurityThread(java.lang.String arg0) throws java.rmi.RemoteException, local.bb.lab.soap.interfaces.PlaceException{
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    placesInterface.startSecurityThread(arg0);
  }
  
  public java.lang.String[] getPlacesCodes() throws java.rmi.RemoteException{
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    return placesInterface.getPlacesCodes();
  }
  
  public local.bb.lab.soap.interfaces.ParkingPlace[] getPlaces() throws java.rmi.RemoteException{
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    return placesInterface.getPlaces();
  }
  
  public void freePlace(java.lang.String arg0) throws java.rmi.RemoteException, local.bb.lab.soap.interfaces.PlaceException{
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    placesInterface.freePlace(arg0);
  }
  
  public java.lang.Boolean isPlaceFree(java.lang.String arg0) throws java.rmi.RemoteException{
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    return placesInterface.isPlaceFree(arg0);
  }
  
  public void takePlace(java.lang.String arg0) throws java.rmi.RemoteException, local.bb.lab.soap.interfaces.PlaceException{
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    placesInterface.takePlace(arg0);
  }
  
  public local.bb.lab.soap.interfaces.ParkingPlace getPlace(java.lang.String arg0) throws java.rmi.RemoteException{
    if (placesInterface == null)
      _initPlacesInterfaceProxy();
    return placesInterface.getPlace(arg0);
  }
  
  
}