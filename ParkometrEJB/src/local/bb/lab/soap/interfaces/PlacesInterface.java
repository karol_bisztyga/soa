/**
 * PlacesInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package local.bb.lab.soap.interfaces;

public interface PlacesInterface extends java.rmi.Remote {
    public void stopSecurityThread(java.lang.String arg0) throws java.rmi.RemoteException, local.bb.lab.soap.interfaces.PlaceException;
    public void startSecurityThread(java.lang.String arg0) throws java.rmi.RemoteException, local.bb.lab.soap.interfaces.PlaceException;
    public java.lang.String[] getPlacesCodes() throws java.rmi.RemoteException;
    public local.bb.lab.soap.interfaces.ParkingPlace[] getPlaces() throws java.rmi.RemoteException;
    public java.lang.Boolean isPlaceFree(java.lang.String arg0) throws java.rmi.RemoteException;
    public local.bb.lab.soap.interfaces.ParkingPlace getPlace(java.lang.String arg0) throws java.rmi.RemoteException;
    public void takePlace(java.lang.String arg0) throws java.rmi.RemoteException, local.bb.lab.soap.interfaces.PlaceException;
    public void freePlace(java.lang.String arg0) throws java.rmi.RemoteException, local.bb.lab.soap.interfaces.PlaceException;
}
