/**
 * PlacesServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package local.bb.lab.soap.services;

public interface PlacesServiceService extends javax.xml.rpc.Service {
    public java.lang.String getPlacesServicePortAddress();

    public local.bb.lab.soap.interfaces.PlacesInterface getPlacesServicePort() throws javax.xml.rpc.ServiceException;

    public local.bb.lab.soap.interfaces.PlacesInterface getPlacesServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
