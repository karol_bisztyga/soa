package local.bb.lab.tools;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import javax.ejb.Stateless;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import local.bb.lab.entities.ParkingPlace;
import local.bb.lab.entities.Ticket;
import local.bb.lab.entities.User;

@Stateless
public class DatabaseTools {

	public static final int SECOND = 1000;
	public static final int MINUTE = SECOND*60;
	public static final int HOUR = MINUTE*60;
	public static final int DAY = HOUR*24;
	
	private SessionFactory factory;
	
	public SessionFactory getSessionFactory() throws IOException {
		if(factory == null) {
			Configuration configuration = new Configuration();
			configuration.setProperty("hibernate.temp.use_jdbc_metadata_defaults","false");
			//entities
			configuration.addAnnotatedClass(User.class);
			configuration.addAnnotatedClass(ParkingPlace.class);
			configuration.addAnnotatedClass(Ticket.class);
			//
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));

			configuration.setProperties(properties);

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().
			applySettings(configuration.getProperties()).build(); 

			factory = configuration.buildSessionFactory(serviceRegistry);
		}
		return factory;
	}
	
	
	
	public String hash(String value) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(value.getBytes());
			byte[] digest = md.digest();
			return String.format("%064x", new java.math.BigInteger(1, digest));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
