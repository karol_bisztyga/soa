package local.bb.lab.tools;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import local.bb.lab.entities.ParkingPlace;
import local.bb.lab.entities.Ticket;
import local.bb.lab.entities.User;
import local.bb.lab.entities.User.ROLES;

public class RebuildDatabase {
	
	public static void main(String[] args) {
		
		try {
			
			Configuration configuration = new Configuration();
			//entities
			configuration.addAnnotatedClass(User.class);
			configuration.addAnnotatedClass(ParkingPlace.class);
			configuration.addAnnotatedClass(Ticket.class);
			//
			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));

			configuration.setProperties(properties);

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().
			applySettings(configuration.getProperties()).build(); 

			SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);

			Session session = factory.getCurrentSession();

			session.getTransaction().begin();

			User user = new User(ROLES.USER, "asd", "asd");
			session.save(new User(ROLES.ADMIN, "admin", "admin"));
			session.save(new User(ROLES.WORKER, "worker", "www"));
			session.save(new User(ROLES.USER, "qwe", "qwe"));
			session.save(user);
			
			ArrayList<ParkingPlace> places = new ArrayList<ParkingPlace>();
			
			places.add(new ParkingPlace(null, "A01"));
			places.add(new ParkingPlace(null, "A02"));
			places.add(new ParkingPlace(null, "B01"));
			places.add(new ParkingPlace(null, "B02"));
			places.add(new ParkingPlace(null, "B03"));
			
			for(ParkingPlace place : places) {
				session.save(place);
			}
			
			Date date = new Date();
			session.save(new Ticket(date, new Date(date.getTime()+DatabaseTools.MINUTE*5), user, places.get(3)));
			session.save(new Ticket(new Date(date.getTime()-DatabaseTools.HOUR*2),
					new Date(date.getTime()-DatabaseTools.HOUR), user, places.get(0)));
			session.save(new Ticket(date, new Date(date.getTime()+DatabaseTools.HOUR*2), user, places.get(1)));
			session.save(new Ticket(new Date(date.getTime()-DatabaseTools.MINUTE*20), 
					new Date(date.getTime()-DatabaseTools.MINUTE*5), user, places.get(2)));
			
			session.getTransaction().commit();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static String hash(String value) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(value.getBytes());
			byte[] digest = md.digest();
			return String.format("%064x", new java.math.BigInteger(1, digest));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
