package local.bb.lab.tools;

public class ParkingPlaceOccupation {
	
	private String code;
	private boolean occupied;
	
	public ParkingPlaceOccupation(String code, boolean occupied) {
		this.code = code;
		this.occupied = occupied;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isOccupied() {
		return occupied;
	}
	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}
	
}
