package local.bb.lab.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;

@Transactional
@Entity(name="ticket")
public class Ticket {
	
	@Id @GeneratedValue
	private int id;
	@Temporal(TemporalType.TIMESTAMP)
	private Date buyDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date expireDate;
	@OneToOne(fetch=FetchType.LAZY, optional=true)
	private User user;
	@OneToOne(fetch=FetchType.LAZY, optional=true)
	private ParkingPlace parkingPlace;
	
	public Ticket() {}

	public Ticket(Date buyDate, Date expireDate, User user, ParkingPlace parkingPlace) {
		this.buyDate = buyDate;
		this.expireDate = expireDate;
		this.user = user;
		this.parkingPlace = parkingPlace;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ParkingPlace getParkingPlace() {
		return parkingPlace;
	}

	public void setParkingPlace(ParkingPlace parkingPlace) {
		this.parkingPlace = parkingPlace;
	}

	@Override
	public String toString() {
		return "Ticket [id=" + id + ", buyDate=" + buyDate + ", expireDate=" + expireDate + ", user=" + user
				+ ", parkingPlace=" + parkingPlace + "]";
	}
	
	
}
