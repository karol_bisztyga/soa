package local.bb.lab.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.transaction.Transactional;

import local.bb.lab.tools.RebuildDatabase;

@Transactional
@Entity(name="users")
public class User {
	
	public enum ROLES {
		GUEST,
		USER,
		ADMIN,
		WORKER
	}
	
	@Id @GeneratedValue @Column(name="user_id")
	private int id;
	private ROLES role;
	private String name;
	private String password;
	
	public User() {}

	public User(ROLES role, String name, String password) {
		this.role = role;
		this.name = name;
		this.setPassword(password);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ROLES getRole() {
		return role;
	}

	public void setRole(ROLES role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = RebuildDatabase.hash(password);
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", role=" + role + ", name=" + name + ", password=" + password + "]";
	}
	
	public static int getRoleWeight(User.ROLES role) {
		if(role == null) {
			return -1;
		}
		switch(role) {
		case GUEST:
			return -1;
		case USER:
			return 1;
		case WORKER:
			return 3;
		case ADMIN:
			return 10;
		}
		return -1;
	}
	
}
