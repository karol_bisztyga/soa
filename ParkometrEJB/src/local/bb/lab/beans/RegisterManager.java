package local.bb.lab.beans;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import local.bb.lab.entities.User;
import local.bb.lab.entities.User.ROLES;
import local.bb.lab.tools.DatabaseTools;

@Stateless
public class RegisterManager {

	@EJB
	private DatabaseTools dbTools;
	
	public boolean register(String name, String password) {
		return register(name, password, ROLES.USER);
	}
	
	public boolean register(String name, String password, ROLES role) {
		if(!isNameFree(name)) {
			return false;
		}
		try {
			Session session = dbTools.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			session.save(new User(role, name, password));
			tx.commit();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean isNameFree(String name) {
		try {
			Session session = dbTools.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			List list = (List) session.createCriteria(User.class).add(
					Restrictions.eq("name", name)).list();
			boolean result = (list.size() == 0);
			tx.commit();
			return result;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
}
