package local.bb.lab.beans;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import local.bb.lab.entities.User;
import local.bb.lab.tools.DatabaseTools;

@Stateless
public class LoginManager {
	
	@EJB
	private DatabaseTools dbTools;
	private User loggedAs = null;
	
	public void login(String userName, String password) {
		try {
			Session session = dbTools.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			List result = (List) session.createCriteria(User.class).add(
					Restrictions.eq("name", userName)).add(Restrictions.eq("password", password)).list();
			if(result.size() == 0) {
				loggedAs = null;
			} else {
				loggedAs = (User)result.get(0);
			}
			tx.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void logout() {
		loggedAs = null;
	}
	
	public User getLoggedAs() {
		return loggedAs;
	}

	public boolean isLoggedIn() {
		return (loggedAs != null);
	}
	
}
