package local.bb.lab.beans;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import local.bb.lab.entities.ParkingPlace;
import local.bb.lab.entities.Ticket;
import local.bb.lab.soap.interfaces.PlacesInterface;
import local.bb.lab.tools.DatabaseTools;

@Stateless
public class ParkingManager {
	
	private final double cashPerHour = 1.75;

	@EJB
	private DatabaseTools dbTools;
	@EJB
	private LoginManager loginManager;
	
	public List<Ticket> getTicketsToShow() {
		try {
			Session session = dbTools.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			Date date = new Date();
			List result = (List) session.createCriteria(Ticket.class).add(
					Restrictions.gt("expireDate", new Date(date.getTime()-DatabaseTools.MINUTE*10)))
					.list();
			tx.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Ticket> getActiveTickets() {
		try {
			Session session = dbTools.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			List result = (List) session.createCriteria(Ticket.class).add(
					Restrictions.gt("expireDate", new Date()))
					.list();
			tx.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<ParkingPlace> getPlaces() {
		try {
			Session session = dbTools.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			List result = (List) session.createCriteria(ParkingPlace.class).list();
			tx.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<ParkingPlace> getFreePlaces() {
		List<ParkingPlace> places = getPlaces();
		List<Ticket> activeTickets = getActiveTickets();
		for(int i=0 ; i<places.size() ; ++i) {
			for(int j=0 ; j<activeTickets.size() ; ++j) {
				if(activeTickets.get(j).getParkingPlace().equals(places.get(i))) {
					places.remove(i);
					break;
				}
			}
		}
		return places;
	}
	
	public boolean isPlacePaid(String code) {
		List<Ticket> tickets = getActiveTickets();
		for(int i=0 ; i<tickets.size() ; ++i) {
			if(tickets.get(i).getParkingPlace().getCode().equals(code)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean buyTicketForPlace(String placeCode, Date buyDate, 
			Date expireDate, PlacesInterface placesService) {
		if(!loginManager.isLoggedIn()) {
			return false;
		}
		List<ParkingPlace> freePlaces = getFreePlaces();
		for(int i=0 ; i<freePlaces.size() ; ++i) {
			if(freePlaces.get(i).getCode().equals(placeCode)) {
				Ticket ticket = new Ticket(buyDate, expireDate, loginManager.getLoggedAs(), freePlaces.get(i));
				try {
					Session session = dbTools.getSessionFactory().openSession();
					Transaction tx = session.beginTransaction();
					tx.begin();
					session.save(ticket);
					tx.commit();
					placesService.stopSecurityThread(placeCode);
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}
				return false;
			}
		}
		return false;
	}

	public Date countTicketExpireDate(double cash) {
		return countTicketExpireDate(cash, new Date());
	}
	
	public Date countTicketExpireDate(double cash, Date buyDate) {
		return new Date(buyDate.getTime() + (long)(DatabaseTools.HOUR*cash/cashPerHour));
	}
	
	public Ticket getCurrentTicket() {
		if(!loginManager.isLoggedIn()) {
			return null;
		}
		try {
			Session session = dbTools.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			tx.begin();
			List result = (List) session.createCriteria(Ticket.class)
					.add(Restrictions.eq("user", loginManager.getLoggedAs()))
					.add(Restrictions.gt("expireDate", new Date()))
					.list();
			if(result.size() == 0) {
				return null;
			}
			Ticket ticket = (Ticket) result.get(0);
			tx.commit();
			return ticket;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
