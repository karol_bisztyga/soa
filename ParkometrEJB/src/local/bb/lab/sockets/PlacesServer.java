package local.bb.lab.sockets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@Stateless
@ServerEndpoint("/sockets/places")
public class PlacesServer {
	
	private List<Session> sessions = new ArrayList<Session>();
	
	@OnOpen
	public void onOpen(Session session){
		sessions.add(session);
	    //System.out.println(session.getId() + " has opened a connection");
    }
	 
    @OnMessage
    public void onMessage(String message, Session session){
        //System.out.println("Message from " + session.getId() + ": " + message);
	}
	
	@OnClose
	public void onClose(Session session){
		sessions.remove(session);
	    //System.out.println("Session " +session.getId()+" has ended");
	}
	
	public void notifyChange() {
		for(int i=0 ; i<sessions.size() ; ++i ) {
			try {
				sessions.get(i).getBasicRemote().sendText("change");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
