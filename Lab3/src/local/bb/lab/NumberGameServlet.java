package local.bb.lab;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.model.NumberGameModel;

public class NumberGameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/numberGame.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Integer number = Integer.parseInt(request.getParameter("number"));
		String result = "";
		int guess = NumberGameModel.getInstance().guess(number);
		if(guess == 0) {
			result = "You guessed it! It's "+ NumberGameModel.getInstance().getCurrentNumber() +
					". Attempts: " + NumberGameModel.getInstance().getShots();
			NumberGameModel.getInstance().reset();
		} else if(guess == 1) {
			result = "Miss at attempt "+ NumberGameModel.getInstance().getShots() +"! Your number " + number + " is too large";
		} else {
			result = "Miss at attempt "+ NumberGameModel.getInstance().getShots() +"! Your number " + number + " is too small";
		}
		out.print(result);
		out.flush();
	}

}
