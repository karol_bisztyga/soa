package local.bb.lab;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.model.FileManager;
import local.bb.lab.model.GuestBookManager;
import local.bb.lab.model.GuestBookManager.Entry;

public class GuestBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FileManager.getInstance().read();
		request.setAttribute("entries", GuestBookManager.getInstance().getEntries());
		request.getRequestDispatcher("/guestBook.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action != null) {
			String responseString = "fail";
			Integer id = Integer.parseInt(request.getParameter("id"));
			if(action.equals("edit")) {
				String newText = request.getParameter("newText");
				if(GuestBookManager.getInstance().editEntry(id, newText)) {
					responseString = "success";
					FileManager.getInstance().write();
				}
			} else if(action.equals("editLock")) {
				if(GuestBookManager.getInstance().lockEntry(id)) {
					responseString = "granted";
				}
			}
			
			PrintWriter out = response.getWriter();
			out.print(responseString);
			out.flush();
		} else {
			String name = request.getParameter("name");
			String text = request.getParameter("text");
			if(name != null && !name.equals("") && text != null && !text.equals("")) {
				GuestBookManager.getInstance().addEntry(new Entry(new Date(), name, text));
				FileManager.getInstance().write();
			}
			doGet(request, response);
		}
	}

}
