package local.bb.lab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.model.LoginManager;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action != null) {
			if(action.equals("logout")) {
				LoginManager.getInstance().logout(request);
				response.sendRedirect("/Lab3/");
				return;
			}
		}
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String login = request.getParameter("login");
			String password = request.getParameter("password");
			if(LoginManager.getInstance().login(request, login, password)) {
				response.sendRedirect("/Lab3/");
				return;
			} else {
				throw new Exception("could not login");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			response.sendRedirect("/Lab3/");
		}
	}

}
