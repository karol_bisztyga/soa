package local.bb.lab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.model.CurrencyModel;
import local.bb.lab.model.LoginManager;
import local.bb.lab.model.exception.CurrencyException;

public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("login", LoginManager.getInstance().loggedIn(request));
		try {
			System.out.println(CurrencyModel.getInstance(this).calculate(12.99, "dollar", "zloty"));
			System.out.println(CurrencyModel.getInstance(this).calculate(56.87, "zloty", "pound"));
			System.out.println(CurrencyModel.getInstance(this).calculate(23.76, "pound", "euro"));
		} catch (CurrencyException e) {
			System.err.println(e.getMessage());
		}
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
