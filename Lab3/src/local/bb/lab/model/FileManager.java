package local.bb.lab.model;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

public class FileManager {
	
	private static final String path = "entries.txt";
	private final Gson gson = new Gson();
	private static FileManager instance;
	
	private FileManager() {}
	
	public static FileManager getInstance() {
		if(instance == null) {
			instance = new FileManager();
		}
		return instance;
	}
	
	public void write() {
		FileWriter out = null;
		try {
			out = new FileWriter(path);
			out.write(gson.toJson(GuestBookManager.getInstance()));
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void read() {
		String resultString = "";
		FileReader in = null;
		try {
			File file = new File(path);
			file.createNewFile();
			in = new FileReader(path);
			//System.out.println("absolute path: " + file.getAbsolutePath());
			int i;
			while((i = in.read()) != -1) {
				resultString += (char)(i);
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		GuestBookManager gbm = (GuestBookManager)gson.fromJson(resultString, GuestBookManager.class);
		if(gbm != null) {
			GuestBookManager.redefine(gbm);
		}
	}
	
}
