package local.bb.lab.model;

import java.util.ArrayList;

import local.bb.lab.model.BeerAdvisorModel.Beer.COLOR;

public class BeerAdvisorModel {
	
	private final ArrayList<Beer> beers = new ArrayList<>();
	
	public BeerAdvisorModel() {
		beers.add(new Beer("Zimny Lech", Beer.COLOR.ZIELONE));
		beers.add(new Beer("Debowe", Beer.COLOR.CIEMNE));
		beers.add(new Beer("Zywiec", Beer.COLOR.JASNE));
		beers.add(new Beer("Redds", Beer.COLOR.OWOCOWE));
	}

	public Beer getBeerOfColor(String colorString) {
		return getBeerOfColor(COLOR.valueOf(colorString.toUpperCase()));
	}
	
	public Beer getBeerOfColor(Beer.COLOR color) {
		for(Beer beer : beers) {
			if(beer.getColor() == color) {
				return beer;
			}
		}
		return null;
	}
	
	public static class Beer {
		public enum COLOR {
			CIEMNE, JASNE, OWOCOWE, ZIELONE
		}
		
		private final String name;
		private final COLOR color;
		public Beer(String name, COLOR color) {
			this.name = name;
			this.color = color;
		}
		public String getName() {
			return name;
		}
		public COLOR getColor() {
			return color;
		}
	}
	
}
