package local.bb.lab.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ProductsManager {

	private static ProductsManager instance;
	private final ArrayList<Product> products;
	
	private ProductsManager() {
		products = new ArrayList<>();
		try {
			File file = new File("products.xml");
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
			        .newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(file);
			int lenght = document.getElementsByTagName("id").getLength();
			for(int i=0 ; i<lenght ; ++i) {
				Integer id = Integer.parseInt(document.getElementsByTagName("id").item(i).getTextContent());
				String name = document.getElementsByTagName("name").item(i).getTextContent();
				Integer price = Integer.parseInt(document.getElementsByTagName("price").item(i).getTextContent());
				products.add(new Product(id, name, price));
			}
		} catch(IOException | ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
	}
	
	public Product getById(int id) {
		for(Product p : products) {
			if(p.getId() == id) {
				return p;
			}
		}
		return null;
	}
	
	public ArrayList<Product> getProducts() {
		return products;
	}

	public static ProductsManager getInstance() {
		if(instance == null) {
			instance = new ProductsManager();
		}
		return instance;
	}
	
	public static class Product {
		private final int id;
		private final String name;
		private final int price;
		public Product(int id, String name, int price) {
			this.id = id;
			this.name = name;
			this.price = price;
		}
		public int getId() {
			return id;
		}
		public String getName() {
			return name;
		}
		public int getPrice() {
			return price;
		}
	}
}
