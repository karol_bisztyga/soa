package local.bb.lab.model;

import java.util.ArrayList;

public class MovieManager {
	
	private static MovieManager instance;
	private final ArrayList<Movie> movies;
	
	private MovieManager() {
		movies = new ArrayList<>();
		movies.add(new Movie("Matrix", "sci-fi", 2000, 100000000));
		movies.add(new Movie("Godfellas", "gangsta", 1993, 120000000));
		movies.add(new Movie("Thin red line", "war", 1995, 90000000));
	}
	
	public ArrayList<Movie> getMovies() {
		return movies;
	}

	public static MovieManager getInstance() {
		if(instance == null) {
			instance = new MovieManager();
		}
		return instance;
	}
	
	public static class Movie {
		private final String title;
		private final String genre;
		private final int year;
		private final int income;
		public Movie(String title, String genre, int year, int income) {
			this.title = title;
			this.genre = genre;
			this.year = year;
			this.income = income;
		}
		public String getTitle() {
			return title;
		}
		public String getGenre() {
			return genre;
		}
		public int getYear() {
			return year;
		}
		public int getIncome() {
			return income;
		}
	}
	
}
