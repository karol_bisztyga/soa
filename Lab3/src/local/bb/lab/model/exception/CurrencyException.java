package local.bb.lab.model.exception;

public class CurrencyException extends Exception {
	
	public CurrencyException(String s) {
		super(s);
	}
}
