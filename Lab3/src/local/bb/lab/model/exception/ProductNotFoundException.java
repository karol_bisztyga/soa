package local.bb.lab.model.exception;

public class ProductNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public ProductNotFoundException(String s) {
		super(s);
	}
}
