package local.bb.lab.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.http.HttpServlet;

import local.bb.lab.model.exception.CurrencyException;

public class CurrencyModel {
	
	private static CurrencyModel instance;
	private final ArrayList<Currency> currencies = new ArrayList<>();
	
	private CurrencyModel(HttpServlet servlet) {
		try {
			Properties prop = new Properties();
			prop.load(servlet.getServletContext().getResourceAsStream("/resources/currency.properties"));
			Enumeration<?> e = prop.propertyNames();
			while(e.hasMoreElements()) {
				String key = (String)e.nextElement();
				double value = Double.parseDouble(prop.getProperty(key));
				currencies.add(new Currency(key, value));
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public static CurrencyModel getInstance(HttpServlet servlet) {
		if(instance == null) {
			instance = new CurrencyModel(servlet);
		}
		return instance;
	}
	
	private Currency getCurrencyByName(String name) {
		for(Currency c : currencies) {
			if(c.getName().equals(name)) {
				return c;
			}
		}
		return null;
	}

	public double calculate(double value, String cinStr, String coutStr) throws CurrencyException {
		Currency cin = getCurrencyByName(cinStr);
		Currency cout = getCurrencyByName(coutStr);
		if(cin == null || cout == null) {
			return 0;
		}
		return calculate(value, cin, cout);
	}
	
	public double calculate(double value, Currency cin, Currency cout) throws CurrencyException {
		if(!currencies.contains(cin) || currencies.contains("cout")) {
			throw new CurrencyException("no such currency");
		}
		double result = value * (cout.getAbsolutePower()/cin.getAbsolutePower());
		result *= 100;
		result = (int)result;
		result /= 100;
		return result;
	}
	
	public static class Currency {
		private final double absolutePower;
		private final String name;
		public Currency(String name, double absolutePower) {
			this.absolutePower = absolutePower;
			this.name = name;
		}
		public double getAbsolutePower() {
			return absolutePower;
		}
		public String getName() {
			return name;
		}
	}
	
}
