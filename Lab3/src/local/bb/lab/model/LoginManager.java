package local.bb.lab.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import local.bb.lab.model.LoginManager.DisplayableAccount;

public class LoginManager {
	
	private final static String LOGIN_SESSION_SIGNATURE = "login";
	
	private static LoginManager instance;
	private ArrayList<Account> accounts = new ArrayList<>();
	
	private LoginManager() {
		accounts.add(new Account("qweqwe", "qweqwe"));
		accounts.add(new Account("asdasd", "asdasd"));
	}
	
	public static LoginManager getInstance() {
		if(instance == null) {
			instance = new LoginManager();
		}
		return instance;
	}
	
	public boolean login(HttpServletRequest request, String loginString, String passString) {
		for(Account account : this.accounts) {
			if(account.getLogin().equals(loginString) && account.getPassword().equals(passString)) {
				request.getSession().setAttribute(LOGIN_SESSION_SIGNATURE, loginString);
				account.setLastLogin(new Date());
				return true;
			}
		}
		return false;
	}
	
	public String loggedIn(HttpServletRequest request) {
		try{
			String login = (String) request.getSession().getAttribute(LOGIN_SESSION_SIGNATURE);
			return login;
		} catch (NullPointerException e) {
			return null;
		}
	}
	
	public void logout(HttpServletRequest request) {
		request.getSession().removeAttribute(LOGIN_SESSION_SIGNATURE);
	}

	public ArrayList<DisplayableAccount> getAccounts() {
		return this.getAccounts("asc");
	}
	
	public ArrayList<DisplayableAccount> getAccounts(String sort) {
		ArrayList<DisplayableAccount> result = new ArrayList<>();
		for(Account a : this.accounts) {
			result.add(new DisplayableAccount(a));
		}
		if(sort.equals("asc")) {
			result.sort(DisplayableAccount.comparatorInc);
		} else if(sort.equals("desc")) {
			result.sort(DisplayableAccount.comparatorDesc);
		}
		return result;
	}
	
	private static class Account {
		private final String login;
		private final String password;
		private Date lastLogin;
		public Account(String login, String password) {
			this.login = login;
			this.password = password;
		}
		public String getLogin() {
			return login;
		}
		public String getPassword() {
			return password;
		}
		public Date getLastLogin() {
			return lastLogin;
		}
		public void setLastLogin(Date lastLogin) {
			this.lastLogin = lastLogin;
		}
	}
	
	public static class DisplayableAccount {
		private final String login;
		private final Date lastLogin;
		public DisplayableAccount(Account a) {
			this.login = a.getLogin();
			this.lastLogin = a.getLastLogin();
		}
		public Date getLastLogin() {
			return lastLogin;
		}
		public String getLogin() {
			return login;
		}
		
		public static final Comparator<DisplayableAccount> comparatorInc = new Comparator<LoginManager.DisplayableAccount>() {
			@Override
			public int compare(DisplayableAccount o1, DisplayableAccount o2) {
				return o1.getLogin().compareTo(o2.getLogin());
			}
		};
		public static final Comparator<DisplayableAccount> comparatorDesc = new Comparator<LoginManager.DisplayableAccount>() {
			@Override
			public int compare(DisplayableAccount o1, DisplayableAccount o2) {
				return -o1.getLogin().compareTo(o2.getLogin());
			}
		};
	}
	
}
