package local.bb.lab.model;

import java.util.Random;

public class NumberGameModel {
	
	private int currentNumber;
	private Random random;
	private int shots;

	private static NumberGameModel instance;
	
	private NumberGameModel() {
		this.random = new Random();
		this.reset();
	}
	
	public void reset() {
		currentNumber = random.nextInt(100) + 1;
		shots = 0;
	}
	
	public int guess(int n) {
		System.out.println("guess " + n + "?" + this.currentNumber);
		++shots;
		if(n == currentNumber) {
			return 0;
		} else if(n > currentNumber) {
			return 1;
		} else {
			return -1;
		}
	}
	
	public int getCurrentNumber() {
		return currentNumber;
	}

	public int getShots() {
		return shots;
	}

	public static NumberGameModel getInstance() {
		if(instance == null) {
			instance = new NumberGameModel();
		}
		return instance;
	}
}
