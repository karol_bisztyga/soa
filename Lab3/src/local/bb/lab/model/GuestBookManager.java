package local.bb.lab.model;

import java.util.Date;
import java.util.Vector;

public class GuestBookManager {
	
	private final Vector<Entry> entries = new Vector<>();
	private static GuestBookManager instance;
	
	private GuestBookManager(){}
	
	private GuestBookManager(GuestBookManager copyFrom) {
		this.entries.clear();
		for(Entry e : copyFrom.getEntries()) {
			this.addEntry(e);
		}
	}
	
	public static GuestBookManager redefine(GuestBookManager copyFrom) {
		instance = new GuestBookManager(copyFrom);
		return instance;
	}
	
	public static GuestBookManager getInstance() {
		if(instance == null) {
			instance = new GuestBookManager();
		}
		return instance;
	}
	
	public void addEntry(Entry e) {
		this.entries.add(e);
	}
	
	public Vector<Entry> getEntries() {
		return entries;
	}
	
	public boolean lockEntry(int id) {
		for(Entry e : this.getEntries()) {
			if(e.getId() == id) {
				if(e.isEditLocked()) {
					return false;
				}
				e.setEditLocked(true);
				return true;
			}
		}
		return false;
	}
	
	public boolean editEntry(int id, String newText) {
		for(Entry e : this.getEntries()) {
			if(e.getId() == id) {
				e.setText(newText);
				e.setEditLocked(false);
				return true;
			}
		}
		return false;
	}

	public static class Entry {
		private static int currentId = 0;
		
		private final int id;
		private final Date date;
		private final String author;
		private String text;
		private boolean editLocked = false;
		
		public Entry(Date date, String author, String text) {
			this.id = ++currentId;
			this.date = date;
			this.author = author;
			this.text = text;
		}
		public int getId() {
			return id;
		}
		public Date getDate() {
			return date;
		}
		public String getAuthor() {
			return author;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public boolean isEditLocked() {
			return editLocked;
		}
		public void setEditLocked(boolean editLocked) {
			this.editLocked = editLocked;
		}
	}
	
}
