package local.bb.lab;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.beans.BucketBean;
import local.bb.lab.model.exception.ProductNotFoundException;

public class ShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/shop.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String result = "";
		try {
			Integer id = Integer.parseInt(request.getParameter("id"));
			Integer number = Integer.parseInt(request.getParameter("number"));
			Object beanObj = request.getSession().getAttribute("bucket");
			BucketBean bb;
			if(beanObj != null) {
				bb = (BucketBean)beanObj;
			} else {
				bb = new BucketBean();
			}
			bb.addProduct(id, number);
			request.getSession().setAttribute("bucket", bb);
			result = "added " + number + " elements";
		} catch(NumberFormatException | ProductNotFoundException e) {
			result = "could not add product";
		}
		out.print(result);
		out.flush();
	}

}
