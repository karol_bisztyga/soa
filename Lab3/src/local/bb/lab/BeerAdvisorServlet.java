package local.bb.lab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.model.BeerAdvisorModel;
import local.bb.lab.model.LoginManager;
import local.bb.lab.model.BeerAdvisorModel.Beer;

public class BeerAdvisorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("login", LoginManager.getInstance().loggedIn(request));
		request.getRequestDispatcher("/beerChoice.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = "";
		try {
			String colorString = request.getParameter("color");
			String ageString = request.getParameter("age");
			if(colorString.length() == 0 || ageString.length() == 0) {
				throw new NullPointerException();
			}
			Integer age = Integer.parseInt(ageString);
			if(age < 18) {
				throw new Exception("Niepelnoletni nie powinni pic alkoholu");
			}
			BeerAdvisorModel beerModel = new BeerAdvisorModel();
			Beer beer = beerModel.getBeerOfColor(colorString);
			message = beer.getName();
		} catch (NullPointerException e) {
			message = "nie podano wszystkich danych!";
		}catch (Exception e) {
			message = "Wystapil blad! " + e.getMessage();
		}
		request.getRequestDispatcher("/beerChoice.jsp?msg="+message).forward(request, response);
	}

}
