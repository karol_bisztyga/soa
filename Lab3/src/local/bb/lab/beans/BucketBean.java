package local.bb.lab.beans;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import local.bb.lab.model.ProductsManager;
import local.bb.lab.model.ProductsManager.Product;
import local.bb.lab.model.exception.ProductNotFoundException;

@Stateless
@LocalBean
public class BucketBean {
	
	private final Map<Integer, Integer> products;

	public BucketBean() {
		products = new HashMap<Integer, Integer>();
		for(Product p : ProductsManager.getInstance().getProducts()) {
			products.put(p.getId(), 0);
		}
	}
	
	public void addProduct(int id, int value) throws ProductNotFoundException {
		addProduct(id, value, false);
	}
	
	public void addProduct(int id, int value, boolean overwrite) throws ProductNotFoundException {
		if(ProductsManager.getInstance().getById(id) == null) {
			throw new ProductNotFoundException("product with id " + id + " not found");
		}
		if(overwrite) {
			products.put(id, value);
		} else {
			products.put(id, value+products.get(id));
		}
		
	}

	public Map<Integer, Integer> getProducts() {
		return products;
	}
	
}
