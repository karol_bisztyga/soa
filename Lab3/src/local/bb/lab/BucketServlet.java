package local.bb.lab;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import local.bb.lab.beans.BucketBean;
import local.bb.lab.model.ProductsManager;
import local.bb.lab.model.exception.ProductNotFoundException;

public class BucketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object beanObj = request.getSession().getAttribute("bucket");
		BucketBean bb;
		if(beanObj != null) {
			bb = (BucketBean)beanObj;
		} else {
			bb = new BucketBean();
		}
		request.getSession().setAttribute("bucket", bb);
		request.setAttribute("bucket", bb.getProducts());
		request.setAttribute("products", ProductsManager.getInstance().getProducts());
		request.getRequestDispatcher("/bucket.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String productsString = request.getParameter("products");
		System.out.println(productsString);
		Gson gson = new Gson();
		ParsedProduct[] arr = gson.fromJson(productsString, ParsedProduct[].class);
		Object beanObj = request.getSession().getAttribute("bucket");
		BucketBean bb;
		if(beanObj != null) {
			bb = (BucketBean)beanObj;
		} else {
			bb = new BucketBean();
		}
		for(ParsedProduct pp : arr) {
			System.out.println(pp.id + "/" + pp.value);
			try {
				bb.addProduct(pp.id, pp.value, true);
			} catch (ProductNotFoundException e) {
				e.printStackTrace();
			}
		}
		request.getSession().setAttribute("bucket", bb);
		
		out.print("ok");
		out.flush();
	}
	
	class ParsedProduct {
		int id;
		int value;
	}
}
