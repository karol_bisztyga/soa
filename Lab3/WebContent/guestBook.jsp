<%@page import="local.bb.lab.model.GuestBookManager"%>
<%@page import="local.bb.lab.model.GuestBookManager.Entry"%>
<%@page import="java.util.Vector"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	leave a comment...<br />
	<form method="POST" action="">
		name: <input name="name" /><br />
		text: <textarea name="text" ></textarea><br />
		<input type="submit" /><br />
	</form>
	<hr />
	<h1>entries:</h1><br />
	<%
		Vector<Entry> list = GuestBookManager.getInstance().getEntries();
			for(Entry entry : list) { %>
			<div id="div-<%= entry.getId() %>">
				<h2><%= entry.getAuthor() %>(<%= entry.getDate() %>)</h2>
				<input type="button" value="edit" id="edit-button-<%= entry.getId() %>" class="edit" />
				<p id="p-<%= entry.getId() %>" style="display:block"><%= entry.getText() %></p>
				<textarea id="text-<%= entry.getId() %>" style="display:none"></textarea><br />
				<input type="button" id="save-<%= entry.getId() %>" class="save" value="save" style="display: none" />
			</div>
		<% 	}	%>
		
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script>
		$(document).ready(function(){
			
			var editing = false;
			var hasToReload = false;
			
			$(".edit").click(function() {
				var id = this.id.split("-")[2];
				$.post("/Lab3/guestBook", {
					"action": "editLock",
					"id": id
				},function(recv) {
					if(recv == "granted") {
						if(hasToReload) {
							location.reload();
						}
						if(editing) return;
						editing = true;
						$("#p-"+id).css("display","none");
						$("#save-"+id).css("display","block");
						$("#text-"+id).css("display","block");
						$("#text-"+id).val($("#p-"+id).html());
					} else {
						alert("someone else's editing it at the moment");
						hasToReload = true;
					}
				});
				
			});
			
			$(".save").click(function() {
				var id = this.id.split("-")[1];
				if(!editing) return;
				editing = false;
				
				$.post("/Lab3/guestBook", {
					"action": "edit",
					"id": id,
					"newText": $("#text-"+id).val()
				},function(recv) {
					if(recv == "success") {
						$("#p-"+id).html($("#text-"+id).val());
					} else {
						alert("could not edit");
					}
				});
				$("#p-"+id).css("display","block");
				$("#save-"+id).css("display","none");
				$("#text-"+id).css("display","none");
			});
			
		});
	</script>

</body>
</html>