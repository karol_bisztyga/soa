<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
	<fmt:setLocale value="en_US"/>
	
	<jsp:include page="header.jsp" />
	<h1>list of movies</h1>
	
	<table border="1">
		<tr>
			<td>title</td>
			<td>genre</td>
			<td>year</td>
			<td>income</td>
		</tr>
		<c:forEach items="${movies}" var="entry">
				<tr>
					<td>${entry.getTitle()}</td>
					
						<c:choose>
							<c:when test="${entry.getGenre() == 'war'}">
								<td style="background-color: yellow;">
									${entry.getGenre()}
								</td>
							</c:when>
							<c:otherwise>
								<td>
									${entry.getGenre()}
								</td>
							</c:otherwise>
						</c:choose>
					<td>${entry.getYear()}</td>
					<td>
						<fmt:formatNumber type="currency" value="${entry.getIncome()}" />
					</td>
				</tr>
		</c:forEach>
	</table>

</body>
</html>