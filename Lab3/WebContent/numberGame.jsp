<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<jsp:include page="header.jsp" />
	take a wild guess<br />
	<input type="number" id="number" /><br />
	<input id="btn" type="button" value="guess!" /><br />
	<div id="response"></div>

	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script>
			$(document).ready(function(){
				
				$("#btn").click(function() {
					var num = $("#number").val();
					if(!num.length || isNaN(num)) return;
					$.post("/Lab3/numberGame", {
						number: num
					},function(recv) {
						$("#response").html(recv);
					});
				});
			});
		</script>
</body>
</html>