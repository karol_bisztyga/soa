<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
	<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
	<jsp:include page="header.jsp" />
	
	<h1>Welcome to the shop</h1>
	<h2>available products</h2>
	<span>
		<a href="bucket">bucket</a>
	</span>
	
	<c:import var="products" url="products.xml"/>
	<x:parse xml="${products}" var="products"/>
	<table border="1">
		<tr>
			<td>Name</td>
			<td>Price</td>
			<td>Add to cart</td>
		</tr>
		<x:forEach select="$products/computer_parts/part" var="item">
			 <tr>
			 	<td><x:out select="$item/name" /></td>
				<td><x:out select="$item/price" />$</td>
				<td>
					<input type="number" id="n-<x:out select="$item/id" />" min="0" val="0" />
					<input type="button" value="add" class="add-button" id="add-<x:out select="$item/id" />" />
				</td>
			</tr>
		</x:forEach>
	</table>
	<br />
	<span id="result"></span>
	
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script>
			$(document).ready(function() {
				$(".add-button").click(function() {
					var id = this.id.split("-")[1];
					$.post("/Lab3/shop", {
						id: id,
						number: $("#n-"+id).val()
					},function(recv) {
						$("#result").html(recv);
						setTimeout(function() {
							console.log(id);
							$("#result").html("");
						}, 1000);
					});
				});
			});
		</script>
	
</body>
</html>