<%@tag import="java.util.ArrayList"%>
<%@tag import="local.bb.lab.model.LoginManager"%>
<%@tag import="local.bb.lab.model.LoginManager.DisplayableAccount"%>
<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="sort" required="true" type="java.lang.String" %>
<%@ attribute name="color" required="false" type="java.lang.String" %>

active users:<br />
	<table border="1">
			<tr>
				<td>name</td>
				<td>last login</td>
			</tr>
		<%
		ArrayList<DisplayableAccount> list = LoginManager.getInstance().getAccounts(sort);
			for(DisplayableAccount da : list) { %>
			<tr>
				<td><%= da.getLogin() %></td>
				<td style="color:${color};">
				<% if(da.getLastLogin() != null) { %>
					<%= da.getLastLogin() %>
				<% } %>
				</td>
			</tr>
		<% 	}	%>
		
	</table>
	<br />
	<hr />
	<br />