<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<jsp:include page="header.jsp" />
	<a href="shop">back to the shop</a><br />
	<h1>Bucket:</h1>

	<table border="1">
		<tr>
			<td>name</td>
			<td>price per 1</td>
			<td>value</td>
			<td>total price</td>
		</tr>
		<c:set var="totalPrice" scope="session" value="${0}"/>
		<c:forEach items="${bucket}" var="entry">
			<c:if test="${entry.value > 0}">
				<tr>
					<c:set var="sumPrice" scope="session" value="${products[entry.key].getPrice()*entry.value}"/>
					<td>${products[entry.key].getName()}</td>
					<td>${products[entry.key].getPrice()}</td>
					<td>
						<input type="number" min="0" id="n-${entry.key}" class="value" value="${entry.value}" />
					</td>
					<td>${sumPrice}</td>
					<c:set var="totalPrice" scope="session" value="${totalPrice + sumPrice}"/>
				</tr>
			</c:if>
		</c:forEach>
	</table>
	<input type="button" value="save" id="btn-save" />
	<br />
	<span>
		total price: <span>${totalPrice}</span>
	</span>
	
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script>
			$(document).ready(function() {
				$("#btn-save").click(function() {
					var products = [];
					$(".value").each(function() {
						var val = $(this).val();
						var id = $(this).attr("id").split("-")[1];
						products[products.length] = {id:id,value:val};
					});
					console.log(products);
					$.post("/Lab3/bucket", {
						products: JSON.stringify(products)
					},function(recv) {
						if(recv == 'ok') {
							location.reload();
						}
					});
				});
			});
		</script>
	

</body>
</html>