package local.bb.lab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jws.WebService;

import local.bb.lab.interfaces.Exchange;

@WebService(endpointInterface="local.bb.lab.interfaces.Exchange")
public class PolishExchange implements Exchange {

	@Override
	public Double getRate(CURRENCY currency) {
		switch(currency) {
		case EUR:
			return 4.18292187;
		case GBP:
			return 4.83754755;
		case USD:
			return 3.7332517;
		}
		return null;
	}

	@Override
	public Double count(CURRENCY currency, double value) {
		return getRate(currency)*value;
	}

	@Override
	public CURRENCY[] getCurrencies() {
		List<CURRENCY> currencies = (List<CURRENCY>) Arrays.asList(CURRENCY.values());
		CURRENCY[] result = new CURRENCY[currencies.size()-1];
		int i=0;
		for(CURRENCY c : currencies) {
			if(c == CURRENCY.PLN) continue;
			result[i++] = c;
		}
		return result;
	}

	@Override
	public CURRENCY getCurrency() {
		return CURRENCY.PLN;
	}
	
	

}
