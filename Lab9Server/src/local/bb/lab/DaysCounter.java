package local.bb.lab;

import java.util.Calendar;

import javax.jws.WebService;

import local.bb.lab.interfaces.Counter;

@WebService(endpointInterface="local.bb.lab.interfaces.Counter")
public class DaysCounter implements Counter {

	@Override
	public long getHolidaysInterval() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, 6);
		c.set(Calendar.DAY_OF_MONTH, 1);
		return (c.getTimeInMillis() - Calendar.getInstance().getTimeInMillis()) / (24 * 60 * 60 * 1000);
	}
	
}
