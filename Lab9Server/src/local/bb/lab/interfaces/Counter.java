package local.bb.lab.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface Counter {
	
	@WebMethod
	public long getHolidaysInterval();
	
}
