package local.bb.lab.interfaces;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface Exchange {
	
	public enum CURRENCY {
		USD,
		EUR,
		PLN,
		GBP
	}
	
	@WebMethod
	public Double getRate(CURRENCY currency);
	@WebMethod
	public Double count(CURRENCY currency, double value);
	@WebMethod
	public CURRENCY[] getCurrencies();
	@WebMethod
	public CURRENCY getCurrency();
	
}
