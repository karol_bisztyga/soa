package local.bb.lab;

import javax.xml.ws.Endpoint;

public class Main {

	public static void main(String[] args) {
		try {
			Endpoint.publish("http://localhost:4572/lab9/counter", new DaysCounter());
			Endpoint.publish("http://localhost:4572/lab9/exchange", new PolishExchange());
			System.out.println("published");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
