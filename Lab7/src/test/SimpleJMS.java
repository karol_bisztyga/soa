package test;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;


public class SimpleJMS {

	public static void main(String[] args) {
		new Thread(new Sender()).start();
		new Thread(new Receiver(1)).start();
		new Thread(new Receiver(2)).start();
	}
	
	public static class Sender implements Runnable {
		@Override
		public void run() {
			try {
				InitialContext ctx = new InitialContext();
				ConnectionFactory factory = (ConnectionFactory) ctx.lookup("ConnectionFactory");
				QueueConnection qc = (QueueConnection) factory.createConnection();
				QueueSession session = (QueueSession) qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
				Queue q = null;
				try {
					q = (Queue) ctx.lookup("queue/ChatQueue");
				} catch(Exception e) {
				}
				if(q==null) {
					q = (Queue) session.createQueue("queue/ChatQueue");
				}
				qc.start();
				QueueSender sender = session.createSender(q);
				TextMessage msg;
				for(int i=0;i<10;++i) {
					msg = session.createTextMessage();
					msg.setObjectProperty("id", "1");
					msg.setText("hello " + i);
					sender.send(msg);
					System.out.println("	sended: " + msg);
					Thread.sleep(400);
				}
				for(int i=0;i<2;++i) {
					msg = session.createTextMessage();
					msg.setObjectProperty("id", (i+1)+"");
					msg.setText("over");
					sender.send(msg);
				}
				session.close();
				qc.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static class Receiver implements Runnable {
		public final int id;
		
		public Receiver(int id) {
			this.id = id;
		}

		@Override
		public void run() {
			try {
				InitialContext ctx = new InitialContext();
				ConnectionFactory factory = (ConnectionFactory) ctx.lookup("ConnectionFactory");
				QueueConnection qc = (QueueConnection) factory.createConnection();
				QueueSession session = (QueueSession) qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
				Queue q = null;
				try {
					q = (Queue) ctx.lookup("queue/ChatQueue");
				} catch(Exception e) {
				}
				if(q==null) {
					q = (Queue) session.createQueue("queue/ChatQueue");
				}
				qc.start();
				QueueReceiver qr = session.createReceiver(q, "id='"+ id +"'");
				while(true) {
					TextMessage tm = (TextMessage) qr.receive();
					String msg = tm.getText();
					System.out.println("["+ id +"]received: " + msg);
					if(msg.equals("over")) break;
				}
				session.close();
				qc.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
