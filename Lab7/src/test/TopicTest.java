package test;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;

public class TopicTest {
	
	public static void main(String[] args) {
		new Thread(new Sender()).start();
		new Thread(new Receiver(1)).start();
		new Thread(new Receiver(2)).start();
		
	}
	
	public static class Sender implements Runnable {
		@Override
		public void run() {
			try {
				InitialContext ctx = new InitialContext();
				ConnectionFactory factory = (ConnectionFactory) ctx.lookup("ConnectionFactory");
				TopicConnection tc = (TopicConnection) factory.createConnection();
				TopicSession session = (TopicSession) tc.createSession(false, Session.AUTO_ACKNOWLEDGE);
				Topic t = null;
				try {
					t = (Topic) ctx.lookup("topic/ChatTopic");
				} catch(Exception e) {
				}
				if(t==null) {
					t = (Topic) session.createTopic("topic/ChatTopic");
				}
				tc.start();
				TopicPublisher tp = session.createPublisher(t);
				TextMessage msg;
				for(int i=0;i<10;++i) {
					msg = session.createTextMessage();
					msg.setText("hello " + i);
					tp.send(msg);
					System.out.println("	sended: " + msg);
					Thread.sleep(400);
				}
				msg = session.createTextMessage();
				msg.setText("over");
				tp.send(msg);
				
				session.close();
				tp.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static class Receiver implements Runnable {
		public final int id;
		
		public Receiver(int id) {
			this.id = id;
		}

		@Override
		public void run() {
			try {
				InitialContext ctx = new InitialContext();
				ConnectionFactory factory = (ConnectionFactory) ctx.lookup("ConnectionFactory");
				TopicConnection tc = (TopicConnection) factory.createConnection();
				TopicSession session = (TopicSession) tc.createSession(false, Session.AUTO_ACKNOWLEDGE);
				Topic t = null;
				try {
					t = (Topic) ctx.lookup("topic/ChatTopic");
				} catch(Exception e) {
				}
				if(t==null) {
					t = (Topic) session.createTopic("topic/ChatTopic");
				}
				tc.start();
				TopicSubscriber ts = session.createSubscriber(t);
				TextMessage msg;
				while(true) {
					TextMessage tm = (TextMessage) ts.receive();
					System.out.println("received: " + tm.getText());
					if(tm.getText().equals("over")) break;
				}
				session.close();
				ts.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


}
