package com.example.faces.Szansa;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name="szansa")
@SessionScoped
public class Szansa implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name = "jo� jo�";
	
	public String getName() {
		return name;
	}

	public String wyslij() {
		System.out.println("wysylanieee...");
		if(Math.random() < .2) {
			return "win";
		}
		return "loss";
	}
}
