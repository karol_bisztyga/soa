package local.bb.lab.rest;

import java.rmi.RemoteException;
import java.util.Date;

import javax.ejb.EJB;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.xml.rpc.ServiceException;

import local.bb.lab.beans.ParkingManager;
import local.bb.lab.soap.interfaces.PlacesInterface;
import local.bb.lab.soap.services.PlacesServiceService;
import local.bb.lab.soap.services.PlacesServiceServiceLocator;
import local.bb.lab.sockets.PlacesServer;

@Path("/tickets")
public class TicketService {
	
	@EJB
	ParkingManager parkingManager;
	@EJB
	private PlacesServer placesServer;

	@Path("/{placeCode}/{cash}")
	@PUT
	public String buyTicket(@PathParam("placeCode") String code, @PathParam("cash") double cash) {
		try {
			PlacesServiceService service = new PlacesServiceServiceLocator();
			PlacesInterface places = service.getPlacesServicePort();
			Date buyDate = new Date();
			Date expireDate = parkingManager.countTicketExpireDate(cash, buyDate);
			boolean result = parkingManager.buyTicketForPlace(code, buyDate, expireDate, places);
			if(result) {
				placesServer.notifyChange();
			}
			return (result) ? "y" : "n" ;
		} catch (ServiceException e) {
			e.printStackTrace();
			return "n";
		}
	}
	
}
