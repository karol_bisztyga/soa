package local.bb.lab.soap;

import java.rmi.RemoteException;
import java.util.Scanner;

import javax.xml.rpc.ServiceException;

import local.bb.lab.soap.interfaces.ParkingPlace;
import local.bb.lab.soap.interfaces.PlaceException;
import local.bb.lab.soap.interfaces.PlacesInterface;
import local.bb.lab.soap.services.PlacesServiceService;
import local.bb.lab.soap.services.PlacesServiceServiceLocator;

public class App {

	public static void main(String[] args) {
		
		try {
			PlacesServiceService service = new PlacesServiceServiceLocator();
			PlacesInterface places = service.getPlacesServicePort();
			Scanner scanner = new Scanner(System.in);
			String option = "";
			while(true) {
				System.out.println("parking motion sensor symulator, places:");
				ParkingPlace[] placesCodes = places.getPlaces();
				for(int i=0 ; i<placesCodes.length ; ++i) {
					String busyStr = (placesCodes[i].isBusy()) ? "busy" : "free" ;
					System.out.println(places.getPlacesCodes()[i] + " " + busyStr);
				}
				System.out.println(">>> choose action");
				System.out.println("[1] mark place as busy");
				System.out.println("[2] mark place as free");
				System.out.println("[0] exit");
				option = scanner.nextLine();
				if(option.equals("1") || option.equals("2")) {
					System.out.println("provide place code");
					String code = scanner.nextLine();
					try{
						if(option.equals("1")) {
							places.takePlace(code);
						} else {
							places.freePlace(code);
						}
					} catch (PlaceException e) {
						System.out.println("invalid place code");
					}
				} else if(option.equals("0")) {
					break;
				} else {
					System.out.println("---> invalid input");
				}
			}
		} catch (ServiceException | RemoteException e) {
			e.printStackTrace();
		}
		
	}

}
