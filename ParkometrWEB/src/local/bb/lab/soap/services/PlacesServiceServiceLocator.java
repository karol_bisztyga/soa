/**
 * PlacesServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package local.bb.lab.soap.services;

public class PlacesServiceServiceLocator extends org.apache.axis.client.Service implements local.bb.lab.soap.services.PlacesServiceService {

    public PlacesServiceServiceLocator() {
    }


    public PlacesServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PlacesServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PlacesServicePort
    private java.lang.String PlacesServicePort_address = "http://localhost:4572/parkometr/soap/places";

    public java.lang.String getPlacesServicePortAddress() {
        return PlacesServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PlacesServicePortWSDDServiceName = "PlacesServicePort";

    public java.lang.String getPlacesServicePortWSDDServiceName() {
        return PlacesServicePortWSDDServiceName;
    }

    public void setPlacesServicePortWSDDServiceName(java.lang.String name) {
        PlacesServicePortWSDDServiceName = name;
    }

    public local.bb.lab.soap.interfaces.PlacesInterface getPlacesServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PlacesServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPlacesServicePort(endpoint);
    }

    public local.bb.lab.soap.interfaces.PlacesInterface getPlacesServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            local.bb.lab.soap.services.PlacesServicePortBindingStub _stub = new local.bb.lab.soap.services.PlacesServicePortBindingStub(portAddress, this);
            _stub.setPortName(getPlacesServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPlacesServicePortEndpointAddress(java.lang.String address) {
        PlacesServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (local.bb.lab.soap.interfaces.PlacesInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                local.bb.lab.soap.services.PlacesServicePortBindingStub _stub = new local.bb.lab.soap.services.PlacesServicePortBindingStub(new java.net.URL(PlacesServicePort_address), this);
                _stub.setPortName(getPlacesServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PlacesServicePort".equals(inputPortName)) {
            return getPlacesServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.soap.lab.bb.local/", "PlacesServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.soap.lab.bb.local/", "PlacesServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PlacesServicePort".equals(portName)) {
            setPlacesServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
