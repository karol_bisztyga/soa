package local.bb.lab.beans;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import local.bb.lab.entities.ParkingPlace;
import local.bb.lab.entities.Ticket;
import local.bb.lab.tools.ParkingPlaceOccupation;

@ManagedBean(name="parking")
@SessionScoped
public class Parking {

	@EJB
	private ParkingManager parkingManager;
	
	private List<Ticket> tickets;
	private List<ParkingPlaceOccupation> places;
	private int activeTicketsCount = 0;
	private double cash;
	private Ticket currentTicket;

	public int getActiveTicketsCount() {
		activeTicketsCount = parkingManager.getTicketsToShow().size();
		return activeTicketsCount;
	}
	
	public void setActiveTicketsCount(int activeTicketsCount) {
		this.activeTicketsCount = activeTicketsCount;
	}

	public List<Ticket> getTickets() {
		tickets = parkingManager.getTicketsToShow();
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public List<ParkingPlaceOccupation> getPlaces() {
		if(places == null) {
			places = new ArrayList<>();
		}
		places.clear();
		List<ParkingPlace> pps = parkingManager.getPlaces();
		List<ParkingPlace> freePlaces = parkingManager.getFreePlaces();
		for(ParkingPlace place : pps) {
			boolean occupied = !(freePlaces.contains(place));
			places.add(new ParkingPlaceOccupation(place.getCode(), occupied));
		}
		return places;
	}

	public void setPlaces(List<ParkingPlaceOccupation> places) {
		this.places = places;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}

	public Ticket getCurrentTicket() {
		currentTicket = parkingManager.getCurrentTicket();
		return currentTicket;
	}

	public void setCurrentTicket(Ticket currentTicket) {
		this.currentTicket = currentTicket;
	}
	
}
