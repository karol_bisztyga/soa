package local.bb.lab.beans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import local.bb.lab.tools.DatabaseTools;

@ManagedBean(name="login")
@SessionScoped
public class Login {
	
	@EJB
	private LoginManager loginManager;
	@EJB
	private DatabaseTools dbTools;
	private String userName;
	private String password;
	private String loggedUserName = null;
	
	public String login() {
		loginManager.login(userName, password);
		if(!loginManager.isLoggedIn()) {
			userName = null;
			password = null;
		} else {
			loggedUserName = loginManager.getLoggedAs().getName();
		}
		return "index";
	}
	
	public void logout() {
		loginManager.logout();
		loggedUserName = null;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = dbTools.hash(password);
	}

	public String getLoggedUserName() {
		return loggedUserName;
	}
	
}
