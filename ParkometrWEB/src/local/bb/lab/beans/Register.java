package local.bb.lab.beans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="register")
@SessionScoped
public class Register {
	
	@EJB
	private RegisterManager registerManager;
	
	private String userName;
	private String password;
	private String message;
	
	public String register() {
		boolean registered = true;
		String info = "";
		if(userName == null || password == null || userName.length() == 0 || password.length() == 0) {
			info = "invalid data";
			registered = false;
		}
		if(!registerManager.register(userName, password)) {
			info = "username might be taken";
			registered = false;
		}
		String message = (registered) ? "successfuly registered" : "could not register " + info ;
		setMessage(message);
		return (registered) ? "index" : "register" ;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
