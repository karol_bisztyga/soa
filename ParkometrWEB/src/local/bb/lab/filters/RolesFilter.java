package local.bb.lab.filters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import local.bb.lab.beans.LoginManager;
import local.bb.lab.entities.User;
import local.bb.lab.entities.User.ROLES;


public class RolesFilter implements Filter {
	
	@EJB
	private LoginManager loginManager;
	private Map<String, ROLES> views = new HashMap<>();

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest reqest = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		String url = reqest.getRequestURI();
		String sessionId = (url.indexOf(";") == -1) ? "" : url.substring(url.indexOf(";")) ;
		ROLES roleRequired = getRoleByURL(url);
		if(roleRequired != null) {
			if(!loginManager.isLoggedIn() && roleRequired != ROLES.GUEST) {
				response.sendRedirect(reqest.getServletContext().getContextPath() + "/index.xhtml"+sessionId);
				return;
			}
			ROLES userRole = (loginManager.isLoggedIn()) ? loginManager.getLoggedAs().getRole() : null ;
			int userRoleWeight = User.getRoleWeight(userRole);
			int roleRequiredWeight = User.getRoleWeight(roleRequired);
			if(userRoleWeight < roleRequiredWeight || !sameSign(userRoleWeight, roleRequiredWeight)) {
				response.sendRedirect(reqest.getServletContext().getContextPath() + "/index.xhtml"+sessionId);
				return;
			}
		}
		arg2.doFilter(arg0, arg1);
	}
	
	
	private ROLES getRoleByURL(String url) {
		for (Map.Entry<String, ROLES> entry : views.entrySet()) {
		    //System.out.println(entry.getKey() + "/" + entry.getValue());
			if(url.indexOf(entry.getKey()) >= 0) {
				return entry.getValue();
			}
		}
		return null;
	}
	
	private boolean sameSign(int a, int b) {
		return ((a*b) > 0);
	}
	
	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		views.put("index.xhtml", null);
		views.put("places.xhtml", ROLES.USER);
		views.put("register.xhtml", ROLES.GUEST);
	}
	
	

}
