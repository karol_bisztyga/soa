$(document).ready(function() {
	
	var connection = new WebSocket('ws://localhost:8080/ParkometrWEB/sockets/places');
	
	connection.onopen = function () {
		console.log("open");
	};
	
	connection.onerror = function (error) {
		console.log('WebSocket Error ' + error);
	};
	
	connection.onmessage = function (e) {
		console.log('Server: ' + e.data);
		if(e.data == "change") {
			updateActiveTicketsCount();
    		updatePlaces();
    		updatecurrentTicket();
		}
	};
	
	$("#buy-ticket").click(function(e) {
		console.log("here");
		console.log($("#cash").val() + "/" + $("#place-code").val());
		var cash = $("#cash").val();
		if(cash == null || cash == "" || isNaN(cash)) {
			return;
		}
		var code = $("#place-code").val();
		if(code == null || code == "") {
			return;
		}
		console.log('sending data ' + code + "/" + cash);
		$.ajax({
		    url: 'rest/tickets/'+code+'/'+cash,
		    type: 'PUT',
		    success: function(result) {
		    	if(result == 'y') {
					updateActiveTicketsCount();
		    		updatePlaces();
		    	} else {
		    		console.log("error occured");
		    	}
		    }
		});
	});
	
});