package local.bb.lab.soap.tools;

public class ParkingPlace {
	
	private final String code;
	private boolean busy;
	private Thread securityCallThread;
	
	public ParkingPlace(String code, boolean busy) {
		this.code = code;
		this.busy = busy;
	}
	
	public void cancelThread() {
		if(securityCallThread != null) {
			securityCallThread.interrupt();
		}
		securityCallThread = null;
	}
	
	public void startThread() {
		cancelThread();
		securityCallThread = new Thread(new SecurityCallTask(code));
		securityCallThread.start();
	}

	public boolean isBusy() {
		return busy;
	}

	public void setBusy(boolean busy) {
		this.busy = busy;
	}

	public String getCode() {
		return code;
	}
	
}
