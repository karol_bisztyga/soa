package local.bb.lab.soap.tools;

import java.util.concurrent.TimeUnit;

import local.bb.lab.soap.services.PlacesService;

public class SecurityCallTask implements Runnable {
	
	private final String code;
	
	public SecurityCallTask(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}

	@Override
	public void run() {
		try {
			TimeUnit.MILLISECONDS.sleep(PlacesService.SECURITY_CALL_TIMEOUT);
			callSecurityForPlace(code);
		} catch (InterruptedException e) {
			System.out.println("cancell security on " + getCode());
			//e.printStackTrace();
		}
	}

	public static void callSecurityForPlace(String code) {
		System.out.println("calling security for place " + code);
	}
	
}
