package local.bb.lab.soap;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Endpoint;
import local.bb.lab.soap.services.PlacesService;
import local.bb.lab.soap.tools.ParkingPlace;

public class PlacesMockServer {
	public static void main(String[] args) {
		try {
			List<ParkingPlace> places = new ArrayList<>();
			places.add(new ParkingPlace("A01", false));
			places.add(new ParkingPlace("A02", false));
			places.add(new ParkingPlace("B01", false));
			places.add(new ParkingPlace("B02", false));
			places.add(new ParkingPlace("B03", false));
			
			Endpoint.publish("http://localhost:4572/parkometr/soap/places", new PlacesService(places));
			System.out.println("published");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
