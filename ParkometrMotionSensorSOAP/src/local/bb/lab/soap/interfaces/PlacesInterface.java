package local.bb.lab.soap.interfaces;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import local.bb.lab.exceptions.PlaceException;
import local.bb.lab.soap.tools.ParkingPlace;

@WebService
public interface PlacesInterface {

	@WebMethod
	public void takePlace(String code) throws PlaceException;
	@WebMethod
	public void freePlace(String code) throws PlaceException;
	@WebMethod
	public Boolean isPlaceFree(String code);
	@WebMethod
	public List<ParkingPlace> getPlaces();
	@WebMethod
	public List<String> getPlacesCodes();
	@WebMethod
	public ParkingPlace getPlace(String code);
	@WebMethod
	public void startSecurityThread(String code) throws PlaceException;
	@WebMethod
	public void stopSecurityThread(String code) throws PlaceException;
	
}
