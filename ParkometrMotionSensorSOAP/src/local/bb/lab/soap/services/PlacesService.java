package local.bb.lab.soap.services;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import local.bb.lab.exceptions.PlaceException;
import local.bb.lab.soap.interfaces.PlacesInterface;
import local.bb.lab.soap.tools.ParkingPlace;

@WebService(endpointInterface="local.bb.lab.soap.interfaces.PlacesInterface")
public class PlacesService implements PlacesInterface {
	
	public static final int SECURITY_CALL_TIMEOUT = 10000;
	
	private List<ParkingPlace> places = null;
	
	public PlacesService(List<ParkingPlace> places) {
		this.places = places;
	}

	@Override
	public void takePlace(String code) throws PlaceException {
		for(int i=0 ; i<places.size() ; ++i) {
			if(places.get(i).getCode().equals(code)) {
				places.get(i).setBusy(true);
				places.get(i).startThread();
				return;
			}
		}
		throw new PlaceException();
	}

	@Override
	public void freePlace(String code) throws PlaceException {
		for(int i=0 ; i<places.size() ; ++i) {
			if(places.get(i).getCode().equals(code)) {
				places.get(i).setBusy(false);
				places.get(i).cancelThread();
				return;
			}
		}
		throw new PlaceException();
	}

	@Override
	public Boolean isPlaceFree(String code) {
		for(int i=0 ; i<places.size() ; ++i) {
			if(places.get(i).getCode().equals(code)) {
				return !places.get(i).isBusy();
			}
		}
		return null;
	}

	@Override
	public List<ParkingPlace> getPlaces() {
		return places;
	}

	@Override
	public ParkingPlace getPlace(String code) {
		for(int i=0 ; i<places.size() ; ++i) {
			if(places.get(i).getCode().equals(code)) {
				return places.get(i);
			}
		}
		return null;
	}
	
	@Override
	public List<String> getPlacesCodes() {
		List<String> result = new ArrayList<String>();
		List<ParkingPlace> pl = getPlaces();
		for(int i=0 ; i<pl.size() ; ++i) {
			result.add(pl.get(i).getCode());
		}
		return result;
	}
	
	@WebMethod
	public void startSecurityThread(String code) throws PlaceException {
		for(int i=0 ; i<places.size() ; ++i) {
			if(places.get(i).getCode().equals(code)) {
				places.get(i).startThread();
				return;
			}
		}
		throw new PlaceException();
	}
	
	@WebMethod
	public void stopSecurityThread(String code) throws PlaceException {
		for(int i=0 ; i<places.size() ; ++i) {
			if(places.get(i).getCode().equals(code)) {
				places.get(i).cancelThread();
				return;
			}
		}
		throw new PlaceException();
	}
	
}
