package local.bb.lab.exceptions;

public class PlaceException extends Exception {
	private static final long serialVersionUID = 1L;

	public PlaceException(String s) {
		super(s);
	}
	
	public PlaceException() {
		this("");
	}
	
}
