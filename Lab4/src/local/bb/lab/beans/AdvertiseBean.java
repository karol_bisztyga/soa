package local.bb.lab.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

@ManagedBean(name="ad")
@SessionScoped
public class AdvertiseBean {
	
	private final ArrayList<Ad> ads = new ArrayList<>();
	private final Random random = new Random();
	private Ad currentAd = null;
	
	public void init() {
		ads.add(new Ad("visit our search engine ASAP!", "www.google.com"));
		ads.add(new Ad("try social media!", "www.facebook.com"));
		ads.add(new Ad("listen to the music and watch funny videos!", "www.youtube.com"));
	}
	
	public Ad getAd() {
		if(ads.size() == 0) init();
		int r = random.nextInt(ads.size());
		currentAd = ads.get(r);
		return currentAd;
	}
	
	public String getAdText() {
		getAd();
		return currentAd.getText();
	}
	
	public void click() {
		try {
			currentAd.click();
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			externalContext.redirect("http://"+currentAd.getUrl());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static class Ad {
		private final String text;
		private final String url;
		private int clicks = 0;
		public Ad(String text, String url) {
			this.text = text;
			this.url = url;
		}
		public String getText() {
			return text;
		}
		public String getUrl() {
			return url;
		}
		public int getClicks() {
			return clicks;
		}
		public void click() {
			++this.clicks;
			System.out.println("new click for " + url + ", total clicks: " + clicks );
		}
	}
	
}
