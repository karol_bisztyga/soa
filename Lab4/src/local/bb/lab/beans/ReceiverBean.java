package local.bb.lab.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="receiver")
@SessionScoped
public class ReceiverBean {

	public enum PROVINCE {
		MALOPOLSKIE,
		WIELKOPOLSKIE,
		POMORSKIE
	}
	
	public enum GENDER {
		MALE,
		FEMALE
	}
	
	private String firstName;
	private String lastName;
	private int age;
	private GENDER gender;
	private PROVINCE province;
	
	public String receive() {
		System.out.println("receive: " + firstName + "," + lastName + "," + age);
		return "summary";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public PROVINCE getProvince() {
		return province;
	}

	public void setProvince(PROVINCE province) {
		this.province = province;
	}
	
	public GENDER getGender() {
		return gender;
	}

	public void setGender(GENDER gender) {
		this.gender = gender;
	}
	
	public GENDER[] getGenders() {
		return GENDER.values();
	}

	public PROVINCE[] getProvinces() {
		return PROVINCE.values();
	}
	
}
