package local.bb.lab.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="poll")
@SessionScoped
public class PollBean {
	
	public enum GENDER {
		MALE,
		FEMALE
	}
	
	private boolean newcomer;
	
	/*
	 * for all
	 * */
	private String name;
	private String email;
	private String postCode;
	private int age;
	private GENDER gender;
	private String education;
	private int size;
	
	/*
	 * for clients
	 * */
	private String lastTimeShopping;
	private String satisfied;
	private String opinionAboutWorkers;
	private String comments;
	
	/*
	 * for newcomers
	 * */
	private String cashSpentMonthly;
	private String usuallyShoppingPlace;
	private String shoppingRate;
	private String favouriteColor;
	
	/*
	 * diferent options depending on gender
	 * */
	private String favouriteClothesType;
	
	/*
	 * options
	 * */
	
	public String[] getCashSpentMonthlyOptions() {
		return new String[]{"up to 100","100-500","500-1000","more than 1000"};
	}
	public String[] getUsuallyShoppingPlaceOptions() {
		return new String[]{"brand shop","hipermarket","bazaar","second hand"};
	}
	public String[] getShoppingRateOptions() {
		return new String[]{"everyday","once a week","once a month","couple times a year"};
	}
	public String[] getFavouriteColorOptions() {
		return new String[]{"colorful and bright","gray","black and white","only black"};
	}
	public String[] getFavouriteClothesTypeOptionsWomen() {
		return new String[]{"skirt suits","thisrts","skirts","trousers"};
	}
	public String[] getFavouriteClothesTypeOptionsMen() {
		return new String[]{"trousers","shorts","suits","shorts","ties"};
	}
	public String[] getLastTimeShoppingOptions() {
		return new String[]{"yesterday","last week","last month","couple months ago","don't know"};
	}
	public String[] getSatisfactionOptions() {
		return new String[]{"yes","rather yes","rather no","no","don't know"};
	}
	
	public GENDER[] getGenders() {
		return GENDER.values();
	}
	
	/*
	 * miscellaneous
	 * */
	
	public String send() {
		System.out.println("sended data: " + this.toString());
		return "pollSummary";
	}

	/*
	 * generated methods
	 * */

	@Override
	public String toString() {
		return "PollBean [newcomer=" + newcomer + ", name=" + name + ", email=" + email + ", postCode=" + postCode
				+ ", age=" + age + ", gender=" + gender + ", education=" + education + ", size=" + size
				+ ", lastTimeShopping=" + lastTimeShopping + ", satisfied=" + satisfied + ", opinionAboutWorkers="
				+ opinionAboutWorkers + ", comments=" + comments + ", cashSpentMonthly=" + cashSpentMonthly
				+ ", usuallyShoppingPlace=" + usuallyShoppingPlace + ", shoppingRate=" + shoppingRate
				+ ", favouriteColor=" + favouriteColor + ", favouriteClothesType=" + favouriteClothesType + "]";
	}
	public String getName() {
		return name;
	}

	public boolean isNewcomer() {
		return newcomer;
	}

	public void setNewcomer(boolean newcomer) {
		this.newcomer = newcomer;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public GENDER getGender() {
		return gender;
	}

	public void setGender(GENDER gender) {
		this.gender = gender;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getLastTimeShopping() {
		return lastTimeShopping;
	}

	public void setLastTimeShopping(String lastTimeShopping) {
		this.lastTimeShopping = lastTimeShopping;
	}

	public String getSatisfied() {
		return satisfied;
	}

	public void setSatisfied(String satisfied) {
		this.satisfied = satisfied;
	}

	public String getOpinionAboutWorkers() {
		return opinionAboutWorkers;
	}

	public void setOpinionAboutWorkers(String opinionAboutWorkers) {
		this.opinionAboutWorkers = opinionAboutWorkers;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCashSpentMonthly() {
		return cashSpentMonthly;
	}

	public void setCashSpentMonthly(String cashSpentMonthly) {
		this.cashSpentMonthly = cashSpentMonthly;
	}

	public String getUsuallyShoppingPlace() {
		return usuallyShoppingPlace;
	}

	public void setUsuallyShoppingPlace(String usuallyShoppingPlace) {
		this.usuallyShoppingPlace = usuallyShoppingPlace;
	}

	public String getShoppingRate() {
		return shoppingRate;
	}

	public void setShoppingRate(String shoppingRate) {
		this.shoppingRate = shoppingRate;
	}

	public String getFavouriteColor() {
		return favouriteColor;
	}

	public void setFavouriteColor(String favouriteColor) {
		this.favouriteColor = favouriteColor;
	}

	public String getFavouriteClothesType() {
		return favouriteClothesType;
	}

	public void setFavouriteClothesType(String favouriteClothesType) {
		this.favouriteClothesType = favouriteClothesType;
	}
	
	
	
}
